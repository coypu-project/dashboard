FROM python:3.10

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

EXPOSE 5015

ENV BOKEH_ALLOW_WS_ORIGIN=*

CMD ["python3", "test.py"]
