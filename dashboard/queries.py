import os
import pandas as pd
import requests
from dotenv import load_dotenv
from io import StringIO
import logging
import inspect
import panel as pn

project_root = os.path.dirname(os.path.dirname(__file__))
load_dotenv(os.path.join(project_root, '.env'))

endpoint_url = os.getenv('ENDPOINT_URL')
auth = (os.getenv('ENDPOINT_USER'), os.environ.get('ENDPOINT_PW'))

headers = {
    "Content-Type": "application/sparql-query",
    "Accept": "text/csv"
}

logging.basicConfig(level=logging.DEBUG)#, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

def execute_query(query, queryName):
    logger.debug("Executing query: %s", queryName)
    try:
        response = requests.post(endpoint_url, data=query, headers=headers, auth=auth)
        response.raise_for_status()
        logger.debug("%s executed successfully.", queryName)
    except requests.exceptions.HTTPError as http_err:
        logger.error("HTTP error occurred: %s", http_err)
    except Exception as err:
        logger.error("Other error occurred: %s", err)
    else:
        if response.content:
            return pd.read_csv(StringIO(response.content.decode('utf-8')), sep=",")
        else:
            logger.warning("Received empty response content.")
            return pd.DataFrame()

# Given the eventtype as input - query all disasters with the eventtype
def query_disaster(query_text):
    sparql_query = f"""
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX coy: <https://schema.coypu.org/global#>

    SELECT ?name ?thing
    WHERE {{
      ?thing rdf:type coy:Disaster;
             rdfs:label ?name ;
             rdf:type ?typeName .
      FILTER (?typeName != coy:Disaster && ?typeName != coy:Event)
      FILTER (?typeName = <https://schema.coypu.org/em-dat#{query_text}>)
    }}
    """

    # Sending the request to the endpoint
    return execute_query(sparql_query, inspect.stack()[0][3])

# input is a coypu event - returns POINT or POLYGON of the event. If Event/affected Region doesn't have geometry -> returns geometry of the country

def query_polygon(event_name):
  sparql_query = f"""
  PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX coy: <https://schema.coypu.org/global#>

SELECT ?thing ?name (geof:convexHull(geof:collect(?geo)) as ?hull) ?country
WHERE {{
    BIND(<{event_name}> as ?thing)
      ?thing rdf:type coy:Disaster;
      rdfs:label ?name ;
      rdf:type ?typeName .
  OPTIONAL {{?thing geo:hasGeometry/geo:asWKT ?regionGeo }}  .
  OPTIONAL {{?thing coy:hasCountryLocation ?country }} .
  OPTIONAL {{?country geo:hasGeometry/geo:asWKT ?countryGeo }} .
  OPTIONAL {{?thing coy:hasAffectedRegion ?region .
      {{?region geo:hasGeometry/geo:asWKT ?region_centre_point_wkt }} .
  }}
    BIND(COALESCE(?region_centre_point_wkt, ?regionGeo, ?countryGeo) AS ?geo) .
}}
GROUP BY ?thing ?name ?hull ?country
  """

  df = execute_query(sparql_query, inspect.stack()[0][3])
  # quick fix for the huge polygon that is created if french is affected by an event (creates single polygon out of a multipolygon)
  if df['country'][0] == 'https://data.coypu.org/country/FRA':
    df['hull'] = 'POLYGON((3.588184 50.378992, 4.286023 49.907497, 4.799222 49.985373, 5.674052 49.529484, 5.897759 49.442667, 6.18632 49.463803, 6.65823 49.201958, 8.099279 49.017784, 7.593676 48.333019, 7.466759 47.620582, 7.192202 47.449766, 6.736571 47.541801, 6.768714 47.287708, 6.037389 46.725779, 6.022609 46.27299, 6.5001 46.429673, 6.843593 45.991147, 6.802355 45.70858, 7.096652 45.333099, 6.749955 45.028518, 7.007562 44.254767, 7.549596 44.127901, 7.435185 43.693845, 6.529245 43.128892, 4.556963 43.399651, 3.100411 43.075201, 2.985999 42.473015, 1.826793 42.343385, 0.701591 42.795734, 0.338047 42.579546, -1.502771 43.034014, -1.901351 43.422802, -1.384225 44.02261, -1.193798 46.014918, -2.225724 47.064363, -2.963276 47.570327, -4.491555 47.954954, -4.59235 48.68416, -3.295814 48.901692, -1.616511 48.644421, -1.933494 49.776342, -0.989469 49.347376, 1.338761 50.127173, 1.639001 50.946606, 2.513573 51.148506, 2.658422 50.796848, 3.123252 50.780363, 3.588184 50.378992))'

# Sending the request to the endpoint
  return df

# input is POINT/POLYGON and trade direction - returns weight and value of tradegroups in the country where polygon lays
def query_trade_country(polygon_area, trade_direction):
    sparql_query = f"""
    PREFIX qb: <http://purl.org/linked-data/cube#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX coy: <https://schema.coypu.org/global#>
    PREFIX geo: <http://www.opengis.net/ont/geosparql#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX spatial: <http://jena.apache.org/spatial#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX coy: <https://schema.coypu.org/global#>
    PREFIX coycube: <https://schema.coypu.org/coy-cube#>

    SELECT distinct ?country ?countryName ?year (sum(?value) as ?total_value) (sum(?weight) as ?total_weight) ?tradeGroup {{
      VALUES ?myPolygon {{
          "{polygon_area}"^^geo:wktLiteral
      }}
     BIND("2018"^^xsd:gYear as ?startYear)
     BIND("2022"^^xsd:gYear as ?endYear)
     BIND(<https://data.coypu.org/geoboundaries/DEU/1/Sachsen> as ?state)

      # here starts the query
      # search for countries in our polygon
      GRAPH <https://data.coypu.org/administrative-regions/> {{
          ?country spatial:intersectBoxGeom(?myPolygon) .
      }}
      ?country a coy:Country ;
        geo:hasGeometry/geo:asWKT ?c_lit .
      FILTER(geof:sfIntersects(?c_lit, ?myPolygon))

      # search for trade data between ?state and the found countries
      GRAPH <https://data.coypu.org/genesis/> {{
        ?trade coycube:tradeCountry ?country ; # the countries that are part of our polygon
               qb:dataSet <https://data.coypu.org/datasets/genesis/51000-0036> ; # set destatis dataset
               coycube:tradeDirection <https://data.coypu.org/tradeDirection/{trade_direction}> ; # change import to export for other direction
               coycube:state ?state ; # set to saxony
               coy:hasYear ?year ; # gets filtered later on
               coycube:value ?value ;
               coycube:weight ?weight ;
               coycube:tradeGroup ?tradeGroupRes . # link to nace trade group
      }}
     FILTER(?year <= ?endYear && ?year >= ?startYear)
     ?country rdfs:label ?countryName . # label of country resource
     ?tradeGroupRes skos:prefLabel ?tradeGroup . # label of trade group resource
    }} GROUP BY ?year ?tradeGroupRes ?state ?country ?countryName ?tradeGroup"""

    # Sending the request to the endpoint
    return execute_query(sparql_query, inspect.stack()[0][3])

@pn.cache(to_disk=True)
def query_observation():
  sparql_query = """
  PREFIX qb: <http://purl.org/linked-data/cube#>
  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
  PREFIX coy: <https://schema.coypu.org/global#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
  PREFIX coycube: <https://schema.coypu.org/coy-cube#>
  # tradeDirection;tradeGroup;state;year;totalValue;totalWeight
  SELECT DISTINCT ?tradeCountryIri (?tradeCountry as ?countryName) ?tradeGroup ?state (?hasYear as ?year) ?tradeDirection (sum(?obsValue) as ?totalValue) (sum(?obsWeight) as ?totalWeight) WHERE {
    BIND("2000"^^xsd:gYear as ?startYear)
    BIND("2023"^^xsd:gYear as ?endYear)
    BIND(<https://data.coypu.org/geoboundaries/DEU/1/Sachsen> as ?stateIri)

    GRAPH <https://data.coypu.org/genesis/> {

      ?trade qb:dataSet <https://data.coypu.org/datasets/genesis/51000-0036> ; # set destatis dataset
            coycube:state <https://data.coypu.org/geoboundaries/DEU/1/Sachsen> ; # set to saxony
            coycube:tradeCountry ?tradeCountryIri ; # the countries that are part of our polygon
            coycube:state ?stateIri ; # the countries that are part of our polygon
            coycube:tradeDirection ?tradeDirectionIri ; # change import to export for other direction
            coy:hasYear ?hasYear ; # "2020"^^xsd:int ; # set to year 2020
            coycube:value ?obsValue ;
            coycube:weight ?obsWeight ;
            coycube:tradeGroup ?tradeGroupIri . # link to nace trade group
    }
    FILTER(?hasYear <= ?endYear && ?hasYear >= ?startYear)
    BIND(strafter(str(?tradeDirectionIri), "https://data.coypu.org/tradeDirection/") as ?tradeDirection)
    ?tradeCountryIri rdfs:label ?tradeCountry .
    ?tradeGroupIri skos:prefLabel ?tradeGroup . # label of trade group resource
    ?stateIri rdfs:label ?state .
  } GROUP BY ?tradeCountry ?tradeCountryIri ?state ?hasYear ?tradeGroup ?tradeDirection"""

  # Sending the request to the endpoint
  df1 = execute_query(sparql_query, inspect.stack()[0][3])

  # Add country coordinate
  df2 = query_country()
  for row in df2.index:
    if df2['countryIri'][row] == 'https://data.coypu.org/country/ATA':
        df2.at[row,'coordinates'] = 'Point(166.676 -77.846)'
  df1 = df1.merge(df2, how= 'left', left_on='tradeCountryIri', right_on='countryIri',  validate='m:1')
  df1 = df1.drop('countryIri', axis=1)

  return df1

@pn.cache(to_disk=True)
def query_country():
  sparql_query = """
  PREFIX geo: <http://www.opengis.net/ont/geosparql#>
  SELECT DISTINCT ?countryIri (min(?coordinate) as ?coordinates) WHERE {
    graph <https://data.coypu.org/countries/>
      { ?countryIri geo:hasGeometry/geo:asWKT ?coordinate }
  } GROUP BY ?countryIri"""

  # Sending the request to the endpoint
  return execute_query(sparql_query, inspect.stack()[0][3])

def query_events():
  sparql_query = """
  PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
  PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
  PREFIX geo: <http://www.opengis.net/ont/geosparql#>
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX coy: <https://schema.coypu.org/global#>

  SELECT DISTINCT ?eventIri ?eventName ?eventType ?eventTypeName ?eventLocation ?countryIri ?year
  WHERE {
    { SELECT DISTINCT ?eventType WHERE { ?eventType rdfs:subClassOf* coy:Disaster
      FILTER(!strstarts(str(?eventType), "https://schema.coypu.org/global#")) }}
    ?eventIri rdf:type ?eventType ;
          rdfs:label ?eventName ;
          coy:hasCountryLocation ?countryIri .

    OPTIONAL { ?eventIri coy:hasYear ?hasYear }
    OPTIONAL { ?eventIri geo:hasGeometry/geo:asWKT ?eventLocation }
    OPTIONAL { ?eventIri coy:hasStartTimestamp ?startTimestamp }
    OPTIONAL { ?eventIri coy:hasStartDate ?startDate }
    OPTIONAL { ?eventIri coy:hasEndTimestamp ?endTimestamp }
    OPTIONAL { ?eventIri coy:hasEndDate ?endDate }

    BIND(COALESCE(xsd:date(?startDate), xsd:date(?startTimestamp)) as ?start)
    BIND(COALESCE(xsd:gYear(?startDate), xsd:gYear(?startTimestamp)) as ?startYear)
    BIND(COALESCE(xsd:gMonth(?startDate), xsd:gMonth(?startTimestamp)) as ?startMonth)
    BIND(COALESCE(xsd:gDay(?startDate), xsd:gDay(?startTimestamp)) as ?startDay)
    BIND(COALESCE(xsd:gYear(?endDate), xsd:gYear(?endTimestamp)) as ?endYear)
    BIND(COALESCE(xsd:gMonth(?endDate), xsd:gMonth(?endTimestamp)) as ?endMonth)
    BIND(COALESCE(xsd:gDay(?endDate), xsd:gDay(?endTimestamp)) as ?endDay)
    BIND(COALESCE(xsd:date(?endDate), xsd:date(?endTimestamp)) as ?end)
    BIND(COALESCE((xsd:int(?endYear)), (xsd:int(?startYear)), ?hasYear) as ?year)

    ?eventType rdfs:label ?eventTypeName .
    GRAPH <https://data.coypu.org/countries/>
      { ?countryIri rdfs:label?countryName}
  }"""

  # Sending the request to the endpoint
  return execute_query(sparql_query, inspect.stack()[0][3])

@pn.cache(to_disk=True)
def query_employees():
  sparql_query = """
  PREFIX coy: <https://schema.coypu.org/global#>
  PREFIX qb: <http://purl.org/linked-data/cube#>
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX cq: <https://schema.coypu.org/coy-cube#>

  SELECT * WHERE {
    GRAPH <https://data.coypu.org/genesis/> {
    ?obs qb:dataSet <https://data.coypu.org/datasets/genesis/81000-0015> ;
        coy:hasYear ?year ;
        cq:erw063 ?employees ;
        cq:tradeGroup ?tradeGroup ;
    }
  }"""

  # Sending the request to the endpoint
  return execute_query(sparql_query, inspect.stack()[0][3])

def query_countries_for_geo(geo_object):
    sparql_query = f"""
    PREFIX coy: <https://schema.coypu.org/global#>
    PREFIX geo: <http://www.opengis.net/ont/geosparql#>
    PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

    SELECT distinct ?countryIri {{
      VALUES ?myPolygon {{
          "{geo_object}"^^geo:wktLiteral
      }}

      ?countryIri a coy:Country ;
        geo:hasGeometry/geo:asWKT ?c_lit .

      FILTER(geof:sfIntersects(?c_lit, ?myPolygon))
    }}"""

    # Sending the request to the endpoint
    return execute_query(sparql_query, inspect.stack()[0][3])


# Query 1 to obtain list of countries
def query_countrylist():
  sparql_query = f"""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX geo: <http://www.opengis.net/ont/geosparql#>
    SELECT DISTINCT ?countryIri ?label
    WHERE {{
      GRAPH <https://data.coypu.org/countries/> {{
          ?countryIri rdfs:label ?label
        }}}}
        GROUP BY ?countryIri ?label
        """

  return(execute_query(sparql_query, inspect.stack()[0][3]))

# Query 2 to optain events for selected country
# Problem - Multipolygons are returned, so the right one needs to be chosen (happens for e.g. USA or France)
def query_eventlist_countrybased(country):
  sparql_query = f"""
  PREFIX geo: <http://www.opengis.net/ont/geosparql#>
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX coy: <https://schema.coypu.org/global#>
  PREFIX emdat: <https://schema.coypu.org/em-dat#>

  SELECT DISTINCT ?name ?thing ?year ?geo
  WHERE {{
      BIND(<{country}> AS ?country)
      ?thing rdf:type coy:Disaster;
            rdfs:label ?name ;
            rdf:type ?typeName ;
            coy:hasCountryLocation ?country ;
            coy:hasYear ?year .
    OPTIONAL {{?thing geo:hasGeometry/geo:asWKT ?regionGeo }}  .
    OPTIONAL {{?thing coy:hasCountryLocation ?country }} .
    OPTIONAL {{?country geo:hasGeometry/geo:asWKT ?countryGeo }} .
    OPTIONAL {{?thing coy:hasAffectedRegion ?region .
        {{?region geo:hasGeometry/geo:asWKT ?region_centre_point_wkt }} .
    }}
      BIND(COALESCE(?region_centre_point_wkt, ?regionGeo, ?countryGeo) AS ?geo) .
  }}
  GROUP BY ?thing ?name ?geo ?year
  """

  return(execute_query(sparql_query, inspect.stack()[0][3]))

# Query 3 to optain affected companies (only french data for now available)
# very slow -> Cached
@pn.cache(to_disk=True)
def query_companies(country="https://data.coypu.org/country/FRA"):
  sparql_query = f"""
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX coy: <https://schema.coypu.org/global#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX coycube: <https://schema.coypu.org/coy-cube#>

SELECT ?label ?coord ?industryLabel ?industryBroad
WHERE {{
     BIND(<{country}> AS ?country)
     ?s a coy:Company ;
      coy:hasEstablishment ?e ;
      coy:hasCountryLocation ?country .
    ?e coy:hasIndustry ?industry .
    ?e rdfs:label ?label .
    ?e geo:hasGeometry/geo:asWKT ?coord .
    ?industry skos:prefLabel ?industryLabel .
  {{
  SELECT DISTINCT ?tradeGroupIri WHERE {{
    GRAPH <https://data.coypu.org/genesis/> {{
      ?trade coycube:state <https://data.coypu.org/geoboundaries/DEU/1/Sachsen> ; # set to saxony
             coycube:tradeGroup ?tradeGroupIri . # link to nace trade group
      }}
    }}
  }}
  BIND(URI(REPLACE(STR(?industry), ".{3}$", "")) AS ?industryBroad)
  FILTER(?industryBroad = ?tradeGroupIri)
  }}
  """

  return(execute_query(sparql_query, inspect.stack()[0][3]))

