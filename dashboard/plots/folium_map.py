from dashboard.plots.helpers import get_disaster_data, get_report_data
from dashboard.queries import query_polygon, query_trade_country
from dashboard.plots.tabulator import table_plot_folium, tabulator

from shapely.geometry import Polygon, Point
import geopandas as gpd
import pandas as pd
pd.set_option('display.max_columns', None)  # Show all columns

import panel as pn
pn.extension('floatpanel')

import folium
import numpy as np

# Retrieve session-specific instances
report_data = get_report_data()
disaster_data = get_disaster_data()
    
event_and_sub_categories = {
    'Biological disaster': ['Epidemic'],
    'Climatological disaster': ['Drought' , 'Glacial lake outburst', 'Wildfire'], 
    'Geophysical disaster': ['Ground movement' , 'Tsunami', 'Volcanic activity'], 
    'Hydrological disaster': ['Avalanche' , 'Flood'], 
    'Meteorological disaster' :['Cold wave', 'Hot wave', 'Storm'],
}

eventDataDF = pd.DataFrame()
tradeGroupList = pd.DataFrame()
tradeCountryList = pd.DataFrame()

# Create LoadingSpinner widgets
loading = pn.indicators.LoadingSpinner(value=False, size=40, name='Query Status Indicator!', color='info', bgcolor='light')
loading_traced_data = pn.indicators.LoadingSpinner(value=False, size=40, name='Query Status Indicator!', color='info', bgcolor='light')

# Create Tabulator widgets
table_widget_eventData = tabulator(eventDataDF)
table_affectedTradeGroups = table_plot_folium(tradeGroupList)
table_affectedCountries = table_plot_folium(tradeCountryList)

# table_affectedTradeGroups.sizing_mode = "stretch_both"
# table_affectedCountries.sizing_mode = "stretch_both"

config = {"headerControls": {"close": "remove", "maximize": "remove"}}

floatpanel = pn.layout.FloatPanel(
    table_affectedCountries, table_affectedTradeGroups,
    name='Affected Country & Trade Groups', margin=20, height=550, width=450, config=config, sizing_mode="stretch_both"
)

table_title = f"""### <span style="color:#2d5986;">Data Table for Trade Groups and Countries Affected by Disaster Event Location(s):</span>"""

initial_map = folium.Map(location=[41.01384, 28.94966], zoom_start=3)
folium_pane = pn.pane.plot.Folium(initial_map, width=900, height=500) # width=1030, height=650

def folium_map_initial():
    table_pane = pn.Column(
        pn.Row(folium_pane, floatpanel),
        table_title, table_widget_eventData
    )
    return table_pane

def show_multichoice_values():
    global eventDataDF

    eventDataDF = pd.DataFrame()
    eventDF = pd.DataFrame()

    selected_events = disaster_data.selected_events
    event_list = disaster_data.newEventListDF

    for i in selected_events:
        index = event_list[event_list['name'] == i].index[0] 
        polygon_df = query_polygon(event_list.at[index, 'thing'])
        polygon_df.rename(columns={'hull': 'geometry'}, inplace=True)

        if polygon_df['geometry'].empty:
            continue
        else: 
            polygon_str = str(polygon_df['geometry'].iloc[0])

            if polygon_str.startswith("POINT"):
                # Parse the point string to extract coordinates
                point_data = polygon_str.replace("POINT(", "").replace(")", "").split()
                longitude, latitude = map(float, point_data)

                # Define the central point
                point = Point(longitude, latitude)

                # Parameters for the circle
                radius_km = 5
                num_vertices = 36  # Ensure at least 4 coordinates for the polygon

                # Convert radius to degrees (approximately)
                radius_degrees = radius_km / 111.32  # Rough approximation

                # Generate points for the polygon (circle approximation)
                angles = np.linspace(0, 2 * np.pi, num_vertices, endpoint=False)
                polygon_points = [
                    (
                        point.x + (radius_degrees / np.cos(np.radians(point.y))) * np.cos(angle),
                        point.y + radius_degrees * np.sin(angle)
                    ) for angle in angles
                ]

                # Close the polygon by repeating the first point at the end
                polygon_points.append(polygon_points[0])

                polygon = Polygon(polygon_points)
                polygon_gdfs = gpd.GeoDataFrame(geometry=[polygon])
                polygon_gdfs.crs = 'epsg:4326'

                # Calculate the centroid without swapping x and y
                centroid = polygon_gdfs.geometry.centroid

                # Add related data to the GeoDataFrame
                polygon_gdfs['name'] = polygon_df['name']
                polygon_gdfs['centroid'] = centroid
                polygon_gdfs['thing'] = polygon_df['thing']

            elif polygon_str.startswith("POLYGON"):
                # Extract coordinates from the POLYGON string
                polygon_coords_str = polygon_str.replace("POLYGON((", "").replace("))", "")
                coords_pairs = polygon_coords_str.split(", ")
                coords_list = [tuple(map(float, pair.split())) for pair in coords_pairs]

                # Create a Shapely Polygon object
                polygon_shapely = Polygon(coords_list)

                # Convert the polygon to a GeoDataFrame
                polygon_gdfs = gpd.GeoDataFrame(geometry=[polygon_shapely])
                polygon_gdfs.crs = 'epsg:4326'

                # Calculate the centroid without swapping x and y
                centroid = polygon_gdfs.geometry.centroid

                # Add related data to the GeoDataFrame
                polygon_gdfs['name'] = polygon_df['name']
                polygon_gdfs['centroid'] = centroid
                polygon_gdfs['thing'] = polygon_df['thing']

            eventDF = pd.concat([eventDF, polygon_gdfs], ignore_index=True)

            # Retrieve trade data
            dfAffectedAreaImport = query_trade_country(polygon_str, "import")
            dfAffectedAreaExport = query_trade_country(polygon_str, "export")
            column_mapping = {
                'country': 'countryIRI',
                'countryName': 'Country',
                'tradeGroup': 'Trade Group',
                'year': 'Year',
                'total_value': 'Value (Tsd. Euro)',
                'total_weight': 'Weight (tons)'
            }

            dfAffectedAreaImport = dfAffectedAreaImport.rename(columns=column_mapping)
            dfAffectedAreaExport = dfAffectedAreaExport.rename(columns=column_mapping)

            dfAffectedAreaImport.drop(columns=['countryIRI'], inplace=True) 
            dfAffectedAreaExport.drop(columns=['countryIRI'], inplace=True)

            dfAffectedAreaImport['Trade Direction'] = "import"
            dfAffectedAreaExport['Trade Direction'] = "export"

            dfAffectedArea = pd.concat([dfAffectedAreaImport, dfAffectedAreaExport], ignore_index=True)
            dfAffectedArea = dfAffectedArea[['Country', 'Trade Direction', 'Trade Group', 'Year', 'Value (Tsd. Euro)', 'Weight (tons)']]

            dfAffectedArea['Year'] = dfAffectedArea['Year'].astype(int)
            # Drop rows where both value and weight are 0.0
            dfAffectedArea = dfAffectedArea[
                (dfAffectedArea['Value (Tsd. Euro)'] != 0.0) | 
                (dfAffectedArea['Weight (tons)'] != 0.0)
            ]

            eventDataDF = pd.concat([eventDataDF, dfAffectedArea], ignore_index=True)

    return eventDF, eventDataDF

def update_folium_map(eventDF, eventDataDF):
    global folium_pane
    map_data = eventDF
    main_data = report_data.mainData

    # Compute the combined centroid of all event geometries
    combined_centroid = map_data.geometry.unary_union.centroid
    centroid_lat = combined_centroid.y  # Latitude
    centroid_lon = combined_centroid.x  # Longitude

    # Initialize the Folium map with the centroid location
    m = folium.Map(location=[centroid_lat, centroid_lon], zoom_start=3)

    for _, r in map_data.iterrows():
        sim_geo = gpd.GeoSeries(r["geometry"]).simplify(tolerance=0.001)
        geo_j = sim_geo.to_json()
        style_function = lambda x: {
            'fillColor': 'red',
            'color': 'red',
            'weight': 3,
            'fillOpacity': 0.5,
        }
        geo_j = folium.GeoJson(data=geo_j, style_function=style_function)
        folium.Popup(r["name"]).add_to(geo_j)
        geo_j.add_to(m)

    # Add markers for each event
    for _, r in map_data.iterrows():
        lat = r["centroid"].y  # Latitude
        lon = r["centroid"].x  # Longitude
        folium.Marker(
            location=[lat, lon],
            popup=format(r["name"]),
        ).add_to(m)

    folium_pane.object = m  

    # Update the tables
    unique_countries = eventDataDF['Country'].unique()
    unique_country_list = unique_countries.tolist()

    # Filter main_data using the unique list of countries
    filtered_data = main_data[main_data['countryName'].isin(unique_country_list)]

    column_mapping = {
        'tradeDirection': 'Trade Direction',
        'countryName': 'Country',
        'tradeGroup': 'Trade Group',
        'year': 'Year',
        'totalValue': 'Value (Tsd. Euro)',
        'totalWeight': 'Weight (tons)'
    }

    filtered_data = filtered_data.rename(columns=column_mapping)
    filtered_data.drop(columns=['coordinates', 'state'], inplace=True)

    report_data.affectedAreaData = filtered_data
    
    table_widget_eventData.value = filtered_data
    if filtered_data.empty:
        table_affectedTradeGroups.value = pd.DataFrame()
        table_affectedCountries.value = pd.DataFrame()
    else:
        table_affectedTradeGroups.value = pd.DataFrame(filtered_data['Trade Group'].unique(), columns=['Trade Group'])
        table_affectedCountries.value = pd.DataFrame(filtered_data['Country'].unique(), columns=['Country'])

    report_data.report_map = pn.pane.plot.Folium(m, width=800, height=400)

def folium_map():
    # Retrieve the data
    eventDF, eventDataDF = show_multichoice_values()

    # Check if the data is empty
    if eventDF.empty or eventDataDF.empty:
        print("No event data available, returning the initial empty map.")
        # Return the default empty map
        folium_pane.object = folium.Map(location=[41.01384, 28.94966], zoom_start=3)
        table_widget_eventData.value = pd.DataFrame()  # Clear the table
        table_affectedTradeGroups.value = pd.DataFrame()
        table_affectedCountries.value = pd.DataFrame()
    else:
        # If data exists, update the map with the new data
        update_folium_map(eventDF, eventDataDF)
