import pandas as pd
import panel as pn
pn.extension('tabulator')

tabulator_editors = {
    'str': {'type': 'input'},
    'int': {'type': 'number'},
    'float': {'type': 'number'}
}

def table_plot(plots_dataframe):
    # Create a Tabulator widget to display the DataFrame
    tabulator = pn.widgets.Tabulator(plots_dataframe, pagination='remote', page_size=20, width=1300, height=600, formatters={'Year': '{:,.0f}', 'year': '{:,.0f}',},
                                     header_filters=True, 
                                     editors=tabulator_editors)
                                                                                                                             # , formatters={'year': 'integer'}
    return tabulator 

def table_plot_folium(plots_dataframe):
    # Create a Tabulator widget to display the DataFrame
    tabulator = pn.widgets.Tabulator(plots_dataframe, pagination='remote', page_size=20, width=390, height=260, formatters={'Year': '{:,.0f}', 'year': '{:,.0f}',},
                                     header_filters=True,
                                     show_index = False,
                                     editors=tabulator_editors)
                                                                                                                             # , formatters={'year': 'integer'}
    return tabulator 

def tabulator(df):
    if df.empty:
        return pn.widgets.Tabulator(df, pagination='remote', page_size=20, width=1200, height=800, formatters={'Year': '{:,.0f}', 'year': '{:,.0f}',},
                                     header_filters=True,
                                     editors=tabulator_editors)
    else:
        editors = {}
        for column in df.columns:
            column_editor = {
                'type': 'editable',  # Set the type to 'editable'
                'value': False,      # Set the default value to False
            }
            editors[column] = column_editor
            
        df.drop('state', axis=1, inplace=True)
        df.drop('coordinates', axis=1, inplace=True)
        column_order = ['tradeDirection', 'tradeGroup', 'countryName', 'year', 'totalValue', 'totalWeight' ]
        df = df[column_order]

        panel_tabular_data= df.pipe(pn.widgets.Tabulator, pagination='local', page_size = 18, width=1100, height=500, formatters={'year': 'integer'},  editors=editors) #, sizing_mode='stretch_width',

        tabular_year_slider = pn.widgets.RangeSlider(name='Years:', start=df['year'].min(), end=df['year'].max(), value=(df['year'].min(), df['year'].max()), step=1, width=200)
        panel_tabular_data.add_filter(tabular_year_slider, 'year')

        multi_select_state_tabular = pn.widgets.MultiSelect(options=sorted(df['tradeDirection'].unique()), name='Trade Direction')
        panel_tabular_data.add_filter(multi_select_state_tabular, 'tradeDirection')

        multi_select_tg_tabular = pn.widgets.MultiSelect(options=sorted(df['tradeGroup'].unique()), name='Select: Trade Group')
        panel_tabular_data.add_filter(multi_select_tg_tabular, 'tradeGroup')

        # Create a widget with a question mark tooltip
        tooltip = pn.widgets.TooltipIcon(value="Use 'Ctrl' for multiple selection as well as deselect the selected options!")

        filename, button = panel_tabular_data.download_menu(
            text_kwargs={'name': 'Enter filename', 'value': 'Filtered_Data.csv', 'width': 150},
            button_kwargs={'name': 'Download table', 'button_type': 'primary', 'button_style': 'solid'},
        )

        return pn.Row(pn.Column(multi_select_state_tabular, tabular_year_slider, multi_select_tg_tabular,tooltip, pn.Column(filename, button)), panel_tabular_data)


