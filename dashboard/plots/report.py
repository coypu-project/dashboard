import pandas as pd
import panel as pn
import plotly.express as px
import plotly.graph_objects as go
import io

from dashboard.plots.tabulator import table_plot
from dashboard.plots.helpers import get_report_data
from dashboard.plots.choropleth_map import create_trade_choropleth_dashboard

report_data = get_report_data()

empty_report_page = """
There are two ways to generate a report:
1) Using Sidebar Filters:
- Select a "Disaster Type" from the sidebar filters.
- Choose one or multiple events from the "List of Events" for the selected disaster type.
- Click the "Show Events on Map!" button, followed by the "Generate Report!" button.

2) Using the Map Tab:
- Navigate to the "Map" tab.
- To generate a report based on a selected area, use the "Polygon Draw Tool" on the right side of the map to draw a polygon.
- After drawing the polygon, click the "Query: Polygon Area" button.
- Then click the "Generate Report!" button.

The system will generate a report that highlights the affected trade groups and countries within the drawn polygon area or selected disaster event(s).
The report will include a proportional comparison of the five trade groups with the highest value in the selected area, compared to other locations (countries). It will also provide insights into alternative countries, visualized through a choropleth map, for better understanding.
"""

def update_report(status):
    if status == "Update":
        polygon_data = report_data.affectedAreaData

        if polygon_data.empty or polygon_data is None:
            return pn.Column(pn.pane.Markdown(empty_report_page))
        
        polygonResult = table_plot(report_data.affectedAreaData)
        # polygonAreaStr = report_data.polygonString

        # Affected Polygon Area Data on Map
        mapPolygonDF = report_data.affectedAreaData

        # Data used for all tabs after filtering by sidebar widgets (Plot Data)
        plotDataDF = report_data.mainData
   
        # Grouped sidebar filtered data by trade direction (export and import)
        import_data_plotData = plotDataDF[plotDataDF['tradeDirection'] == 'import']
        export_data_plotData = plotDataDF[plotDataDF['tradeDirection'] == 'export'] 

        # Affected Trade Groups Data : export and import (for all different countries)
        import_data_polygon = mapPolygonDF[mapPolygonDF['Trade Direction'] == 'import']
        export_data_polygon = mapPolygonDF[mapPolygonDF['Trade Direction'] == 'export'] 

        # Affected Trade Groups' Total Values (all together, we grouped by Trade Groups, only trade groups with total value)
        affectedTradeGroupsImport = import_data_polygon.groupby('Trade Group', as_index=False)['Value (Tsd. Euro)'].sum()
        affectedTradeGroupsExport = export_data_polygon.groupby('Trade Group', as_index=False)['Value (Tsd. Euro)'].sum()

        # Get Top 5 Trade Groups' Data from affectedTradeGroupsImport or affectedTradeGroupsExport
        topFiveAffectedTradeGroupsImport = affectedTradeGroupsImport.sort_values(by='Value (Tsd. Euro)', ascending=False).head(5)
        topFiveAffectedTradeGroupsExport = affectedTradeGroupsExport.sort_values(by='Value (Tsd. Euro)', ascending=False).head(5)

        # Import : Plot Data Trade Groups' Total Values for Top 5 Affected Trade Groups in Polygon Area (all together, we grouped by Trade Groups, only trade groups with total value)
        importTradeGroupsToCompare = topFiveAffectedTradeGroupsImport['Trade Group'].tolist()
        import_filtered_plot_data = import_data_plotData[import_data_plotData['tradeGroup'].isin(importTradeGroupsToCompare)]
        import_trade_group_sums_plot_data = import_filtered_plot_data.groupby('tradeGroup', as_index=False)['totalValue'].sum()

        # Export : Plot Data Trade Groups' Total Values for Top 5 Affected Trade Groups in Polygon Area (all together, we grouped by Trade Groups, only trade groups with total value)
        exportTradeGroupsToCompare = topFiveAffectedTradeGroupsExport['Trade Group'].tolist()
        export_filtered_plot_data = export_data_plotData[export_data_plotData['tradeGroup'].isin(exportTradeGroupsToCompare)]
        export_trade_group_sums_plot_data = export_filtered_plot_data.groupby('tradeGroup', as_index=False)['totalValue'].sum()

        # Grouping by Trade Group and aggregating unique countries
        grouped_tg_country_import = import_data_polygon.groupby('Trade Group')['Country'].unique().reset_index()
        AffectedTradeGroupsWithCountriesImport = pd.DataFrame({'Trade Group': grouped_tg_country_import['Trade Group'], 'Country': grouped_tg_country_import['Country'].apply(', '.join)})

        grouped_tg_country_export = export_data_polygon.groupby('Trade Group')['Country'].unique().reset_index()
        AffectedTradeGroupsWithCountriesExport = pd.DataFrame({'Trade Group': grouped_tg_country_export['Trade Group'], 'Country': grouped_tg_country_export['Country'].apply(', '.join)})        


        # Initialize an empty list to store dataframes for each trade group
        dfs = []

        ls_tg_topfive = topFiveAffectedTradeGroupsImport['Trade Group'].tolist()

        # Iterate through each trade group in ls_tg_topfive
        for trade_group in ls_tg_topfive:
            # Filter import_data_plotData for the current trade group
            filtered_df = import_data_plotData[import_data_plotData['tradeGroup'] == trade_group]

            # Group by Country and aggregate unique values
            grouped_countries = filtered_df.groupby('tradeGroup')['countryName'].unique().reset_index()

            # Create a dataframe from the grouped data
            df = pd.DataFrame({'Trade Group': grouped_countries['tradeGroup'],
                            'Country': grouped_countries['countryName'].apply(', '.join)})
            # Append the dataframe to the list
            dfs.append(df)

        # Concatenate the list of dataframes into one dataframe, result_df = keeps the alternative countries
        result_df = pd.concat(dfs, ignore_index=True)
        # result_df : these are the countries associated with affected trade groups in affected region

        # these are the affected countries in polygon region
        affectedCountriesImport = import_data_polygon['Country'].unique()
        affectedCountriesExport = export_data_polygon['Country'].unique()

        # Function to remove affected countries from the list of countries
        def remove_affected_countries(countries_list, affected_countries):
            countries = countries_list.split(', ')
            for country in affected_countries:
                if country in countries:
                    countries.remove(country)
            return ', '.join(countries)

        # Apply the function to each row in the DataFrame
        result_df['Country'] = result_df.apply(lambda row: remove_affected_countries(row['Country'], affectedCountriesImport), axis=1)

        #allDataTradeGroups = report_data.affectedAreaData.groupby('tradeGroup')['Value'].sum()

        plot_labels_import = topFiveAffectedTradeGroupsImport['Trade Group'].tolist()
        plot_labels_export = topFiveAffectedTradeGroupsExport['Trade Group'].tolist()

        color_palettes_import = [        
            ['#cc0000', '#FF9B85'],  # Aqua Blue for Hydrogen, complemented with Coral Pink
            ['#cc0000', '#B7E9F7'],  # Grey for Carbon Dioxide, contrasted with Vibrant Green
            ['#cc0000', '#6A0DAD'],  # Green for Nitrogen, contrasted with Deep Violet
            ['#cc0000', '#34568B'],  # Deep Blue for Water, complemented with Golden Yellow for Sunlight
            ['#cc0000', '#8FD694']   # Vibrant Green for Oxygen, complemented with Vivid Magenta for vitality
        ]

        color_palettes_export = [        
            ['#cc0000', '#FFD700'],  # Red contrasted with Gold, enhancing the visual impact
            ['#cc0000', '#2ca02c'],  # Red contrasted with a shade of Green, offering a vibrant contrast
            ['#cc0000', '#1f77b4'],  # Red contrasted with Blue, a classic and effective combination
            ['#cc0000', '#9467bd'],  # Red contrasted with Lavender, for a softer yet distinct pairing
            ['#cc0000', '#ff7f0e']   # Red contrasted with Orange, creating a warm, energetic palette
        ]
        def plot_pie(tg, color_palette, td):
            plot_label_str = str(tg)
            plot_label_x = "Affected region: <br>" + plot_label_str
            plot_label_y = "Other regions: <br>" + plot_label_str

            if td == "import":
                affected_value_tg = topFiveAffectedTradeGroupsImport.loc[topFiveAffectedTradeGroupsImport['Trade Group'] == plot_label_str, 'Value (Tsd. Euro)'].values[0]
                total_value_tg = import_trade_group_sums_plot_data.loc[import_trade_group_sums_plot_data['tradeGroup'] == plot_label_str, 'totalValue'].values[0]

            if td == "export":
                affected_value_tg = topFiveAffectedTradeGroupsExport.loc[topFiveAffectedTradeGroupsExport['Trade Group'] == plot_label_str, 'Value (Tsd. Euro)'].values[0]
                total_value_tg = export_trade_group_sums_plot_data.loc[export_trade_group_sums_plot_data['tradeGroup'] == plot_label_str, 'totalValue'].values[0]

            value1 = affected_value_tg
            value2 = total_value_tg - affected_value_tg

            fig = go.Figure()
            fig.add_trace(go.Pie(labels=[plot_label_x, plot_label_y], 
                                values=[value1, value2],
                                hoverinfo='label+percent+value',
                                domain=dict(x=[0, 0.8]),
                                name="Affected Trade Groups",
                                marker=dict(colors=color_palette,
                                            pattern=dict(shape=[".", "x", "+", "-"])
                                            )
                                )
                        ) 

            # Update layout
            fig.update_layout(
                width=350, 
                height=350,
                legend=dict(
                    orientation="h",
                    yanchor="bottom",
                    y=1.02,
                    xanchor="center",
                    x=0.5,
                    font=dict(size=11,family="Arial")
                ),
                margin=dict(b=100))  # Adjust bottom margin to make space for the legend

            return fig

        pie_plots_import = pn.Row()
        pie_plots_export = pn.Row()

        for index, tg in enumerate(plot_labels_import):
            color_palette = color_palettes_import[index % len(color_palettes_import)]  # Ensuring we loop through color palettes
            plot_tg = plot_pie(tg, color_palette, "import")
            plot_plotly = pn.pane.Plotly(plot_tg)
            pie_plots_import.append(plot_plotly)

        for index, tg in enumerate(plot_labels_export):
            color_palette = color_palettes_export[index % len(color_palettes_export)]  # Ensuring we loop through color palettes
            plot_tg = plot_pie(tg, color_palette, "export")
            plot_plotly = pn.pane.Plotly(plot_tg)
            pie_plots_export.append(plot_plotly)


        textTD= str(report_data.parametersDict['Trade Direction'])
        textTG= str(report_data.parametersDict['Trade Group'])
        textTY= str(report_data.parametersDict['Year'])
        textAV= str(report_data.parametersDict['Value'])
        textAW= str(report_data.parametersDict['Weight'])

        filter_text = f"""
        ### Sidebar Filter Parameters:
        - **Trade Direction(s)**: {textTD}
        - **Trade Group(s)**: {textTG}
        - **Start/End Trade Years**: {textTY}
        - **Trade Value**: {textAV}
        - **Trade Weight**: {textAW} 
        """

        # Create a line separator using HTML markup
        line_separator = pn.pane.HTML("<hr>", width=1800)   

        import_text = f"""
        ### <span style="color:#2d5986;">Affected Trade Groups (Import):</span>  
        - The five highest value trade groups in affected region: 
        {', '.join(topFiveAffectedTradeGroupsImport['Trade Group'])}

        - Proportional comparison of the highest five trade groups in affected region. 
        #### <span style="color:red;">Note:</span> *Values are in Tsd. Euro, and values are given for the sum of last 5 years (from 2018 to 2023).*
        """
        export_text = f"""
        ### <span style="color:#2d5986;">Affected Trade Groups (Export):</span>
        - The five highest value trade groups in affected region: 
        {', '.join(topFiveAffectedTradeGroupsExport['Trade Group'])}

        - Proportional comparison of the highest five trade groups in affected region. 
        #### <span style="color:red;">Note:</span> *Values are in Tsd. Euro, and values are given for the sum of last 5 years (from 2018 to 2023).*
        """
        polygonCountries_text = f"""
        ### <span style="color:#2d5986;">Affected Countries:</span>
        {', '.join(affectedCountriesImport)}
        """        

        table_text = f"""
        ### <span style="color:#2d5986;">Data Table:</span>
        - It shows all trade groups and countries in affected region.
        """

        # Change the dataframe column names
        data_for_choropleth = polygon_data.rename(columns={
            'Country': 'countryName',
            'Trade Group': 'tradeGroup',
            'Year': 'year',
            'Trade Direction': 'tradeDirection',
            'Value (Tsd. Euro)': 'totalValue',
            'Weight (tons)': 'totalWeight'
        })

        main_data = report_data.mainData

        # Convert polygon_data into a set of tuples for filtering
        polygon_data_tuples = set(data_for_choropleth[['countryName', 'tradeGroup', 'year', 'tradeDirection', 'totalValue', 'totalWeight']].apply(tuple, axis=1))

        # Filter out rows from main_data that are present in polygon_data
        main_data_filtered = main_data[~main_data[['countryName', 'tradeGroup', 'year', 'tradeDirection', 'totalValue', 'totalWeight']].apply(tuple, axis=1).isin(polygon_data_tuples)]

        # Filter the main_data to include only the top 5 affected trade groups along with their corresponding data
        main_data_filtered = main_data_filtered[main_data_filtered['tradeGroup'].isin(ls_tg_topfive)]

        # Have choropleth map for top 5 affected countries (instead of table)
        layout_choropleth = create_trade_choropleth_dashboard(main_data_filtered)
        layout_choropleth.pop(0)
        choropleth_map_tradeGroups = layout_choropleth

        pie_import_layout = pn.Column(
            pn.Row(pie_plots_import[0],pn.Spacer(width=20),pie_plots_import[1],pn.Spacer(width=20),pie_plots_import[2]),
            pn.Row(pn.Spacer(width=185),pie_plots_import[3],pn.Spacer(width=27),pie_plots_import[4])
        )

        pie_export_layout = pn.Column(
            pn.Row(pie_plots_export[0],pn.Spacer(width=20),pie_plots_export[1],pn.Spacer(width=20),pie_plots_export[2]),
            pn.Row(pn.Spacer(width=185),pie_plots_export[3],pn.Spacer(width=27),pie_plots_export[4])
        )

        title_choropleth_map = f"""
        ### <span style="color:#2d5986;">Alternative Countries for the 5 Most Affected Trade Groups at the Disaster Location:</span>
        """

        layout = pn.Column(f""" ### <span style="color:#2d5986;">Affected Region:</span>""", report_data.report_map, line_separator, import_text,  pie_import_layout, line_separator, polygonCountries_text, title_choropleth_map, choropleth_map_tradeGroups, line_separator, export_text, pie_export_layout, line_separator, table_text,  polygonResult)
        
        # Specify the function to be called when the button is clicked        
        def download_layout():
            sio = io.StringIO()
            layout.save(sio)
            sio.seek(0)
            return sio

        download = pn.widgets.FileDownload(filename='CoyPu Public Dashboard - Report.html', callback=download_layout, embed=True, button_type='light', button_style='outline')

        download_label =  """
            ### <span style="color:#2d5986;">Save Report:</span>
            """

        return  pn.Column(layout, pn.Row(download_label, download) )
    else:
        return pn.Column(pn.pane.Markdown(empty_report_page))




