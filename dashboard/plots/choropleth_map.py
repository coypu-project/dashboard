import panel as pn
import folium
import pandas as pd
import geopandas as gpd
import json
import pycountry

pn.extension('plotly', 'folium')

def create_trade_choropleth_dashboard(receive_data):

    data = receive_data

    # Extract unique trade groups and years from the provided DataFrame
    unique_trade_groups = sorted(data['tradeGroup'].unique().tolist())
    min_year = data['year'].min()
    max_year = data['year'].max()

    # Widgets using unique values from the DataFrame
    trade_direction_selector = pn.widgets.RadioButtonGroup(name='Trade Direction', options=['import', 'export'], value='import', button_type="primary", button_style="outline")
    trade_group_selector = pn.widgets.Select(name='Trade Group', options=unique_trade_groups, value=unique_trade_groups[0], width=350)
    year_slider = pn.widgets.IntRangeSlider(name='Year Range', start=min_year, end=max_year, value=(min_year, max_year))
    metric_selector = pn.widgets.RadioButtonGroup(name='Metric', options=['Total Value', 'Total Weight'], value='Total Value', button_type="primary", button_style="outline")
    info_icon_input = pn.widgets.TooltipIcon(value="Select a trade direction, trade group, and a range of years to view the data on the map.")
    info_icon_output = pn.widgets.TooltipIcon(value="Select an output metric.")

    # Map the user-friendly metric names to the actual column names
    metric_column_map = {
        'Total Value': 'Total Value (Tsd. Euro)',
        'Total Weight': 'Total Weight (tons)'
    }

    # Function to filter data and create a new Folium choropleth map
    def create_choropleth(trade_direction, trade_group, year_range, metric):
        # Filter data based on the selected trade direction, trade group, and year range
        filtered_data = data[
            (data['tradeDirection'] == trade_direction) &
            (data['tradeGroup'] == trade_group) &
            (data['year'] >= year_range[0]) &
            (data['year'] <= year_range[1])
        ]
        
        # Aggregate the data by country for selected years, trade group, and trade direction
        aggregated_data = filtered_data.groupby('countryName').agg(
            {'totalValue': 'sum', 'totalWeight': 'sum'}
        ).reset_index()
        
        # Rename columns for consistency
        aggregated_data.rename(columns={
            'countryName': 'Country',
            'totalValue': 'Total Value (Tsd. Euro)',
            'totalWeight': 'Total Weight (tons)'
        }, inplace=True)
        
        # Map country names to ISO3 codes
        def get_country_iso3(name):
            replacements = {
                'Turkey': 'Türkiye',
                'Russia': 'Russian Federation',
                'South Korea': 'Korea, Republic of',
                'North Korea': 'Korea, Democratic People\'s Republic of',
                'Syria': 'Syrian Arab Republic',
                'Macedonia': 'North Macedonia',
                'Tanzania': 'Tanzania, United Republic of',
                'United States': 'United States of America',
                'Iran': 'Iran, Islamic Republic of',
                'Venezuela': 'Venezuela, Bolivarian Republic of',
                'Bolivia': 'Bolivia, Plurinational State of',
                'Congo': 'Congo, The Democratic Republic of the',
                'Democratic Republic of the Congo': 'Congo, The Democratic Republic of the',
                'Czech Republic': 'Czechia',
                'Cape Verde': 'Cabo Verde',
                'Ivory Coast': 'Côte d\'Ivoire',
                'Eswatini': 'Swaziland',
                'Burma': 'Myanmar',
            }
            # Replace the country name if it exists in the replacements dictionary
            name = replacements.get(name, name)
            try:
                return pycountry.countries.lookup(name).alpha_3
            except Exception as e:
                print(f"Failed to lookup country: {name}")
                return None

        aggregated_data['iso_a3'] = aggregated_data['Country'].apply(get_country_iso3)
        aggregated_data = aggregated_data.dropna(subset=['iso_a3'])
        
        # Use the mapped column name based on the metric selected
        color_metric = metric_column_map[metric]
        
        # Load Natural Earth data using geopandas
        world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
        
        # Merge world GeoDataFrame with aggregated data
        world = world[['geometry', 'iso_a3', 'name']]
        world_merged = world.merge(aggregated_data, on='iso_a3', how='left')
        
        # Create a Folium Map
        m = folium.Map(location=[20, 0], zoom_start=2)
        
        # Convert GeoDataFrame to GeoJSON
        geo_json_data = json.loads(world_merged.to_json())
        
        # Create the choropleth
        folium.Choropleth(
            geo_data=geo_json_data,
            data=aggregated_data,
            columns=['iso_a3', color_metric],
            key_on='feature.properties.iso_a3',
            fill_color= 'Spectral',
            fill_opacity=0.7,
            line_opacity=0.2,
            legend_name=color_metric,
            highlight=True
        ).add_to(m)
        
        # Add tooltips
        folium.GeoJson(
            geo_json_data,
            style_function=lambda x: {
                'fillColor': 'transparent',
                'color': 'transparent',
                'fillOpacity': 0
            },
            tooltip=folium.features.GeoJsonTooltip(
                fields=['name', color_metric],
                aliases=['Country: ', f'{color_metric}: '],
                localize=True
            )
        ).add_to(m)
        
        return m

    # Bind the function to the widgets
    @pn.depends(
        trade_direction_selector, trade_group_selector, year_slider, metric_selector
    )
    def update_plot(trade_direction, trade_group, year_range, metric):
        folium_map = create_choropleth(trade_direction, trade_group, year_range, metric)
        return pn.pane.plot.Folium(folium_map, width=900, height=500)

    # Layout
    layout = pn.Column(f"""### <span style="color:#2d5986;">Choropleth Map of Global Trade Data for Saxony:</span>""", 
        pn.Row(
            pn.Column(
                pn.Row("Input Variables:", info_icon_input),
                trade_direction_selector,
                trade_group_selector,
                year_slider
            ), 
            pn.Spacer(width=150),
            pn.Column(
                pn.Row("Output Metrics:", info_icon_output),
                metric_selector
            )
        ),
        update_plot
    )

    return layout