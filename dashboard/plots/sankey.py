from bokeh.models import ColumnDataSource
from bokeh.palettes import *
from bokeh.plotting import figure, show
from bokeh.transform import factor_cmap
from math import pi

import plotly.graph_objects as go
import panel as pn
import pandas as pd
import numpy as np

from dashboard.plots.helpers import get_sankey_data, get_report_data

# Retrieve session-specific instances
sankey_data = get_sankey_data()
report_data = get_report_data()

# Layout function
def sankey_plot(df):  
    input_parameters = sankey_data.my_dict      
    tradeDirection = input_parameters.get("trade direction")
    state = "Sachsen"

    if input_parameters.get("trade group") == "None":
        unique_trade_groups = sorted(df['tradeGroup'].unique())
        tradeGroup = unique_trade_groups[0] if unique_trade_groups else None
    else:
        tradeGroup = input_parameters.get("trade group")

    year = input_parameters.get("year")
    output = input_parameters.get("output")

    filtered_df_tg = df[(df['tradeGroup'] == tradeGroup) & 
                                        (df['year'] == year) & 
                                        (df['state'] == state)& 
                                        (df['tradeDirection'] == tradeDirection)]

    # Create a new DataFrame with the necessary columns
    new_df_tg = filtered_df_tg[['countryName', 'tradeGroup', 'state', 'year', 'totalValue', 'totalWeight']]

    report_data.sankeyDF = new_df_tg
 
    trade_groups = sorted(new_df_tg['tradeGroup'].unique())
    countries = sorted(new_df_tg['countryName'].unique())

    # Create a list of node labels by concatenating unique 'tradeGroup' and 'countryName' values
    node_labels = list(trade_groups) + list(countries)

    # Create a mapping from labels to node indices
    label_to_index = {label: index for index, label in enumerate(node_labels)}

    # Initialize empty lists for sources, targets, and values
    sources = []
    targets = []
    values = []

    for _, row in new_df_tg.iterrows():
        source_index = label_to_index[row['tradeGroup']]
        target_index = label_to_index[row['countryName']] + len(trade_groups)
        sources.append(source_index)
        targets.append(target_index-1)
        values.append(row['totalValue'])

    # Create a Sankey diagram using Plotly
    fig = go.Figure(go.Sankey(
        node=dict(
            pad=15,
            thickness=20,
            line=dict(color="black", width=0.3),
            label=node_labels
        ),
        link=dict(
            source=sources,
            target=targets,
            value=values,
            color='#D2E6FF'  # Single color for all links, color='#A6E3D7'
        )
    ))

    # Set nodes as non-editable
    fig.update_traces(arrangement='fixed')

    # Update the plot title based on the selected metrics
    title = f'<b>"{tradeGroup}" <br>{tradeDirection} for {state} in {year}.</b>'
    if output == 'totalWeight':
        title += '<br> (in tons)</b>' 
    else:
        title += '<br> (Tsd. EUR)</b>' 
    fig.update_layout(title=title, title_font={"size": 12})

    panel_layout_sankey =pn.pane.Plotly(fig)
    panel_layout_sankey.height = 1100 
    panel_layout_sankey.width = 650       
    layout = panel_layout_sankey
    
    return layout