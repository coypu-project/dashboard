from bokeh.models import NumeralTickFormatter, FuncTickFormatter, FactorRange
from bokeh.models import ColumnDataSource, TapTool
from bokeh.palettes import Category20, Category10, Category20b
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from math import pi
import panel as pn
from bokeh.models import WheelZoomTool, PanTool
from bokeh.layouts import column, row
from dashboard.plots.helpers import get_sankey_data, get_report_data

# Retrieve session-specific instances
sankey_data = get_sankey_data()
report_data = get_report_data()

# Function to update the dictionary
def update_dict(new_data):
    sankey_data.my_dict = new_data


def bar_chart(df):
    global new_data
    bar_df = df
    new_data = df
    
    if len(bar_df['tradeDirection'].unique()) == 2:
        bar_df = bar_df[(bar_df['tradeDirection'] == "import")]
        tradeDirection = "import"
    elif len(bar_df['tradeDirection'].unique()) > 0 and bar_df['tradeDirection'].unique()[0] == "export":
        tradeDirection = "export"
    else:
        tradeDirection = "import"
    
    bar_df = bar_df[['tradeDirection', 'state', 'tradeGroup','countryName', 'year', 'totalWeight', 'totalValue']]

    def filter_data(bar_df, tradeDirection , year, state):
        new_df = bar_df[(bar_df['tradeDirection'] == tradeDirection) &
                            (bar_df['state'] == state) & 
                            (bar_df['year'] == year)]
        return new_df

    data = filter_data(bar_df, tradeDirection, year = 2020, state = "Sachsen") 
    output = "totalValue"

    if output == 'totalValue':
        measure_col = 'totalValue'
    else:  
        measure_col = 'totalWeight'   

    tradegroups = list (data['tradeGroup'].unique())
    values = data.groupby('tradeGroup')[measure_col].sum()
    global source
    source = ColumnDataSource(data=dict(tradegroups=tradegroups, values=values))

    # Adjusting palette length to accommodate all tradegroups
    num_colors_needed = len(tradegroups)
    palette = Category20b[20] + Category20[20] 
    combined_palette = palette[:num_colors_needed]

    # Create a color mapper using the combined palette
    color_mapper = factor_cmap(field_name='tradegroups', palette=combined_palette, factors=tradegroups)    

    if output == 'totalValue':
        TOOLTIPS = [('Trade Group:', "@tradegroups"), ('value in Tsd. Euro', "@values{0,0.0}"),]
        #title = 'value diagram'
    else:
        TOOLTIPS = [('Trade Group:', "@tradegroups"), ('weight in tons', "@values{0,0.0}"),]
        #title = 'weight diagram'

    global bar_plot
    bar_plot = figure(x_range=tradegroups, height=450,width=1850, tools='hover,reset,wheel_zoom,pan', tooltips= TOOLTIPS ) # , title=title
    bar_plot.vbar(x='tradegroups', top='values', width=0.4, source=source, line_color='white', fill_color=color_mapper) 
    bar_plot.x_range.range_padding = 0.1  # Adjust as needed  
    bar_plot.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    bar_plot.xgrid.grid_line_color = None

    if output == 'value':
        bar_plot.yaxis.axis_label = "value in Tsd. EUR"
    else:
        bar_plot.yaxis.axis_label = "net mass in tons"

    bar_plot.title.text_font_size = '8pt'
    bar_plot.xaxis.axis_label_text_font_size = "9pt"
    bar_plot.xaxis.major_label_text_color = "black" # Change "red" to the color you desire
    bar_plot.yaxis.axis_label_text_font_size = "9pt"
    bar_plot.xaxis.major_label_orientation = pi/6

    bar_plot.xaxis.formatter = FuncTickFormatter(code="""
        var labels = tick.split(' ');
        var max_words = 12;  // Adjust this value based on your preference
        var num_lines = Math.ceil(labels.length / max_words);
        var lines = [];
        for (var i = 0; i < num_lines; i++) {
            var line = labels.slice(i * max_words, (i + 1) * max_words).join(' ');
            lines.push(line);
        }
        return lines.join('\\n');
    """)

    def update_bar_plot(event):
        tradeDirection = trade_direction_widget.value
        year = year_widget.value
        output = output_widget.value
        data = filter_data(new_data, tradeDirection, year, state='Sachsen')
        measure_col = 'totalValue' if output == 'totalValue' else 'totalWeight'
        
        tradegroups = list(data['tradeGroup'].unique())
        values = data.groupby('tradeGroup')[measure_col].sum().tolist()
        source.data = dict(tradegroups=tradegroups, values=values)

        bar_plot.x_range.factors = tradegroups  

        source.selected.on_change('indices', update_label)
        if output == 'totalValue':
            bar_plot.yaxis.axis_label = "value in Tsd. EUR"
        else:
            bar_plot.yaxis.axis_label = "net mass in tons"

    # Adding the Tap tool
    bar_plot.add_tools(TapTool())

    # Define the Panel widgets
    label = pn.widgets.StaticText(name='Selected Trade Group', value='None') 
    trade_direction_widget = pn.widgets.Select(name='Trade Direction', options=sorted(df['tradeDirection'].unique()), width=150)
    output_widget = pn.widgets.RadioButtonGroup(name='Select output to visualize', options=['totalValue', 'totalWeight'], value='totalValue', button_type='primary', button_style='outline')
    year_widget = pn.widgets.Select(name='Year', options=sorted(df['year'].unique()), width=150) #, value=2019


    # Modified callback function to directly reference the label widget
    def update_label(attr, old, new):
        if new:
            selected_index = new[0]
            label.value = source.data['tradegroups'][selected_index]
            label.param.watch(reactive_function, 'value')
        else:
            label.value = "None"
            label.param.watch(reactive_function, 'value')

    source.selected.on_change('indices', update_label)   

    # Use selected bar label as input variable for Sankey diagram
    @pn.depends(label.param.value) # , watch=True
    def reactive_function(value):
        sankey_diagram_input_parameters = {
            "trade direction": trade_direction_widget.value, 
            "trade group": label.value, 
            "year": year_widget.value,
            "output": output_widget.value 
        }        
        update_dict(sankey_diagram_input_parameters) 
        report_data.barChartTradeDirection = trade_direction_widget.value
        report_data.barChartSelectedCategory = label.value

    # Add the callback function to the select_state widget
    trade_direction_widget.param.watch(update_bar_plot, 'value')
    output_widget.param.watch(update_bar_plot, 'value')
    year_widget.param.watch(update_bar_plot, 'value')
    
    trade_direction_widget.param.watch(reactive_function, 'value')
    output_widget.param.watch(reactive_function, 'value')
    year_widget.param.watch(reactive_function, 'value')

    layout_bar_plot = pn.Column(pn.Row(trade_direction_widget, year_widget, pn.Column(pn.Spacer(height=19), output_widget), label), pn.pane.Bokeh(row(bar_plot)), reactive_function)

    return layout_bar_plot

