import pyproj
import panel as pn
import geoviews as gv
import holoviews as hv
import pandas as pd
from bokeh.models import HoverTool, LassoSelectTool, PolyDrawTool
from panel.widgets import Button
from bokeh.models import ColumnDataSource
from cartopy import crs
from dashboard.queries import query_trade_country
from shapely.geometry import Polygon
from dashboard.plots.helpers import get_report_data, get_geo_map_data, get_main_page_layout
from dashboard.plots.report import update_report

# Retrieve session-specific instances
report_data = get_report_data()
geo_map_data = get_geo_map_data()
mainPageLayout = get_main_page_layout()

tabulator_editors = {
    'str': {'type': 'input'},
    'int': {'type': 'number'},
    'float': {'type': 'number'}
}

# Update dynamic polygon coordinates dataframe
def updatePolyDF(new_data):
    geo_map_data.polygonCoordinates = new_data

# Generate paths for a single connection from start coordinate to end coordinat. The higher number of samples the better appreance (but render slow).
def get_circle_path(start, end, samples=200):
    sx, sy = start
    ex, ey = end
    g = pyproj.Geod(ellps='WGS84')
    (az12, az21, dist) = g.inv(sx, sy, ex, ey)
    lonlats = g.npts(sx, sy, ex, ey, samples)
    return [(sx, sy)] + lonlats + [(ex, ey)]

# Generate geo map layout
def create_visualization(hover_df, countries, new_width, new_height):
    centerSaxony = (13.4583, 50.9295)
    paths = []
    
    # For each unique country, we generate connection path and add them to the list.
    for _, details_list in countries.items():
        details = details_list[0]
        coords_str = details[0]  
        lon, lat = map(float, coords_str.split(","))
        coords = (lon, lat)
        paths.append(get_circle_path(centerSaxony, coords))

    # Define Map , OSM = OpenStreetMap
    tiles = gv.tile_sources.OSM()

    path = gv.EdgePaths(paths, kdims=['Longitude', 'Latitude']).opts(color='grey', line_width=1.5)
    points_data = []
    
    # Generate data for the the points on the map. It is used for hover tool on the map as well as accessing detailed data later on to filter.
    for country, details in countries.items():
        coords_str, trade_direction, trade_group, state, year, value, weight = details[0]
        lon, lat = tuple([float(coord) for coord in coords_str.split(",")])

        # Filter DataFrame for specific country
        country_df = hover_df[hover_df['countryName'] == country]
        years = ', '.join(str(year) for year in sorted(country_df['year'].unique()))

        # Calculate unique trade directions
        unique_trade_directions = country_df['tradeDirection'].unique()

        size = len(unique_trade_directions)
        size_int = int(size)
        text = "1"
        numeric_text = int(text)

        info = "Please use 'Lasso Select' tool to select multiple points and view trade data in the table below."

        # # Assign trade directions to variables
        if size_int > numeric_text:
            trade_directions = ' & '.join(unique_trade_directions)
            country_df_import = hover_df[(hover_df['countryName'] == country) & (hover_df['tradeDirection'] == 'import')]
            unique_trade_groups_import = country_df_import['tradeGroup'].nunique()
            total_value_import = country_df_import['totalValue'].sum()
            total_value_import = "{:,.1f}".format(total_value_import)
            country_df_export = hover_df[(hover_df['countryName'] == country) & (hover_df['tradeDirection'] == 'export')]            
            unique_trade_groups_export = country_df_export['tradeGroup'].nunique()
            total_value_export = country_df_export['totalValue'].sum()
            total_value_export = "{:,.1f}".format(total_value_export)
            unique_trade_groups = (f'import: {str(unique_trade_groups_import)}, export: {unique_trade_groups_export}')            
            total_value = (f'import: {total_value_import}, export: {total_value_export}')
            
            points_data.append((lon, lat, country, trade_directions, unique_trade_groups, total_value, years, info))

        else:
            trade_directions_no = f"{unique_trade_directions[0]}"
            unique_trade_groups_no = country_df['tradeGroup'].nunique()
            total_value_no = country_df['totalValue'].sum()
            total_value_no = "{:,.1f}".format(total_value_no)

            # Directly append geographic coordinates as well as the data we would like to see in HoverTool
            points_data.append((lon, lat, country, str(trade_directions_no), str(unique_trade_groups_no), total_value_no, years, info))
     
    # Define vdims to include the new data fields with the updated names
    vdims = ['Country', 'TradeDirections', 'TradeGroups', 'TotalValue', 'Years', 'Info']
    points = gv.Points(points_data, kdims=['Longitude', 'Latitude'], vdims=vdims)
    
    # Define HoverTool properties
    hover = HoverTool()
    hover.tooltips = [
        ("Country", "@Country"), ("Trade Direction(s)", "@TradeDirections"), ("Number of Trade Group(s)", "@TradeGroups"), 
        ("Total Value in Tsd. Euro", "@TotalValue"), ("In Years", "@Years"), ("Note", "@Info"),
    ]
 
    # Define LassoSelectTool 
    lasso = LassoSelectTool()

    # Generate final plot layout with points and return
    plot = tiles * path * points.opts(
        color='green', size=10,
        tools=[hover, lasso],
        hover_line_color='white', hover_fill_color='red', hover_alpha=1,
        active_tools=['wheel_zoom', 'tap']
    ).opts(width=new_width, height=new_height)
    return plot, points 


# Function to create report visualization
def create_report_visualization(polygon_data, width=700, height=500):
    # Check if polygon_data is empty or invalid
    if polygon_data is None or len(polygon_data) == 0:
        return None  # Exit the function if polygon_data is None or empty
    
    # Create a Shapely Polygon to validate the geometry
    polygon = Polygon(polygon_data)
    
    if not polygon.is_valid:
        raise ValueError("The provided polygon coordinates are invalid.")

    # Calculate the centroid and area of the polygon
    centroid = polygon.centroid
    area = polygon.area

    # Convert the polygon into a GeoViews object
    poly_gv = gv.Polygons([{'Longitude': [x[0] for x in polygon_data], 
                            'Latitude': [x[1] for x in polygon_data], 
                            'Area': area}])

    # Check if the centroid has valid coordinates
    if not centroid.is_empty:
        # Add a marker for the centroid
        centroid_gv = gv.Points([(centroid.x, centroid.y)]).opts(
            color='blue', size=10, marker='x'
        )
    else:
        centroid_gv = gv.Points([]).opts()  # Empty points if centroid is invalid

    # Define map tiles (OpenStreetMap)
    tiles = gv.tile_sources.OSM()

    # Combine the tiles, the polygon, and the centroid marker
    report_plot = tiles * poly_gv.opts(
        fill_color='red', 
        fill_alpha=0.5, 
        line_color='black', 
        line_width=2, 
        active_tools=['wheel_zoom'],
        width=width, height=height
    ) * centroid_gv

    return report_plot

# Create a button widget
button_report = pn.widgets.Button(name='Generate Report!', button_type='success', button_style='outline')

# Define the function that will be called when the button is clicked
def call_report_tab(event):
    report_data.report_map = pn.Column(create_report_visualization(geo_map_data.polygonCoordinates.values, width=900, height=450))
    report_panel = mainPageLayout.report_panel
    new_report_panel = update_report("Update")  # Update the report panel
    report_panel.clear()
    report_panel.extend([new_report_panel])
    tabs = mainPageLayout.tabs
    button_report.on_click(lambda event: tabs.param.update(active=4))
    
# Generate dataframe for drawn polygon coordinates by PolyDrawTool
def selected_polygon_info(data):
    if not data or 'xs' not in data or not data['xs'] or 'ys' not in data or not data['ys']:
        return pd.DataFrame(columns=['Longitude', 'Latitude'])
    
    # Store dataframe in dynamic dataframe "polygonDF"
    updatePolyDF(pd.DataFrame({'Longitude': data['xs'][0], 'Latitude': data['ys'][0]}))
    return pd.DataFrame({'Longitude': data['xs'][0], 'Latitude': data['ys'][0]})
    
# Main function
def geomap(df):
    global polygon_stream
    global selection
    global countries
    
    geo_map_data.polygonCoordinates = geo_map_data.polygonCoordinates[0:0]
    geo_map_data.countriesData = geo_map_data.countriesData[0:0]
    
    countries = {}
    for _, row in df.iterrows():
        # Extract values from the row
        country_name = row['countryName']
        coordinates = row['coordinates']
        trade_direction = row['tradeDirection']
        trade_group = row['tradeGroup']
        state = row['state']
        year = int(row['year'])
        total_value = float(row['totalValue'])
        total_weight = float(row['totalWeight'])
        if country_name not in countries:
            countries[country_name] = []
        countries[country_name].append((coordinates, trade_direction, trade_group, state, year, total_value, total_weight))

    plot, points = create_visualization(df, countries, new_width=950, new_height=600)
    #plot, points = create_visualization(df, countries, new_width=1250, new_height=700)

    # Explicitly create PolyDrawTool instance
    poly_draw_tool = PolyDrawTool()

    # Specify tools for the main plot, but not for the PolyDraw layer
    poly_draw = gv.Polygons([]).opts(
        line_width=1, 
        line_color="red", # seagreen
        fill_color="#ffad99",  # This sets the fill color of the drawn polygon, fill_color="mediumturquoise"
        fill_alpha=0.3,
    )

    combined = plot * poly_draw

    selection = hv.streams.Selection1D(source=points)
    polygon_stream = hv.streams.PolyDraw(source=poly_draw)
    
    polygon_table = hv.DynamicMap(lambda data: hv.Table(selected_polygon_info(data), ['Longitude', 'Latitude']), streams=[polygon_stream]).opts(width=200, height=200)

    # Watch polygon_stream data (drawn polygon area) and store any changes in dynamic dataframe 
    @pn.depends(polygon_stream.param.data, watch=True)
    def update_polygon_df(event):
        if event is not None and 'xs' in event and 'ys' in event:
            if event['xs'] and event['ys']:
                geo_map_data.polygonCoordinates = pd.DataFrame({'Longitude': event['xs'][0], 'Latitude': event['ys'][0]})
            else:
                geo_map_data.polygonCoordinates = pd.DataFrame({"Longitude": [], "Latitude": []})
        else:
            pass
    

  # Watch selection from LassoSelectTool and store any changes in dynamic dataframe 
    @pn.depends(selection.param.index, watch=True)
    def update_selection_df(index):
        if index:
            selected_countries = [list(countries.values())[i] for i in index]
            selected_country_names = [list(countries.keys())[i] for i in index]
            country_list = []
            direction_list = []
            group_list = []
            state_list = []
            year_list = []
            value_list = []
            weight_list = []
            for country_data in selected_countries:
                for entry in country_data:
                    country_list.append(selected_country_names[selected_countries.index(country_data)])
                    direction_list.append(entry[1])
                    group_list.append(entry[2])
                    state_list.append(entry[3])
                    year_list.append(str(entry[4]))
                    value_list.append(entry[5])
                    weight_list.append(entry[6])
            data = {
                'Country': country_list,
                'Trade Direction': direction_list,
                'Trade Group': group_list,
                'State': state_list,
                'Year': year_list,
                'Value (Tsd. Euro)': value_list,
                'Weight (tons)': weight_list
            }
            geo_map_data.countriesData = pd.DataFrame(data)
            table_widget_selectedCountries.value = geo_map_data.countriesData
        else:
            # Reset the dataframe if no points are selected
            geo_map_data.countriesData = pd.DataFrame({"Country": [], "Trade Direction": [], "Trade Group": [], "State": [], "Year": [], "Value (Tsd. Euro)": [], "Weight (tons)": []})
            table_widget_selectedCountries.value = geo_map_data.countriesData

    def on_button_click(event):
        coordinates = list(zip(geo_map_data.polygonCoordinates['Longitude'], geo_map_data.polygonCoordinates['Latitude']))

        # Create a Polygon object
        polygonPoints = Polygon(coordinates)

        #Convert Polygon object to the desired format
        geo_map_data.coordStr = 'POLYGON((' + ', '.join([f'{x[0]} {x[1]}' for x in polygonPoints.exterior.coords]) + '))'
        dfAffectedAreaImport= query_trade_country(geo_map_data.coordStr, "import")
        dfAffectedAreaExport= query_trade_country(geo_map_data.coordStr, "export")
        column_mapping = {'country': 'countryIRI',
                        'countryName':'Country',
                        'tradeGroup': 'Trade Group',
                        'year': 'Year',
                        'total_value': 'Value (Tsd. Euro)',
                        'total_weight': 'Weight (tons)'
                        }

        dfAffectedAreaImport = dfAffectedAreaImport.rename(columns=column_mapping)
        dfAffectedAreaExport = dfAffectedAreaExport.rename(columns=column_mapping)

        dfAffectedAreaImport.drop(columns=['countryIRI'], inplace=True) 
        dfAffectedAreaExport.drop(columns=['countryIRI'], inplace=True)

        dfAffectedAreaImport['Trade Direction'] = "import"
        dfAffectedAreaExport['Trade Direction'] = "export"

        dfAffectedArea = pd.concat([dfAffectedAreaImport, dfAffectedAreaExport], ignore_index=True)
        dfAffectedArea = dfAffectedArea[['Country', 'Trade Direction', 'Trade Group', 'Year', 'Value (Tsd. Euro)', 'Weight (tons)']]

        dfAffectedArea = dfAffectedArea[(dfAffectedArea['Value (Tsd. Euro)'] != 0.0) | (dfAffectedArea['Weight (tons)'] != 0.0)]

        dfAffectedArea['Year'] = dfAffectedArea['Year'].astype(int)

        report_data.polygonString = geo_map_data.coordStr

        main_data = report_data.mainData

        # Get unique list of countries
        unique_countries = dfAffectedArea['Country'].unique()
        unique_country_list = unique_countries.tolist()

        # Filter main_data using the unique list of countries
        filtered_data = main_data[main_data['countryName'].isin(unique_country_list)]

        column_mapping = {'tradeDirection': 'Trade Direction',
                        'countryName':'Country',
                        'tradeGroup': 'Trade Group',
                        'year': 'Year',
                        'totalValue': 'Value (Tsd. Euro)',
                        'totalWeight': 'Weight (tons)'
                        }

        filtered_data = filtered_data.rename(columns=column_mapping)

        filtered_data.drop(columns=['coordinates'], inplace=True) 
        filtered_data.drop(columns=['state'], inplace=True)        
        
        table_widget_affectedTradeGroups.value = filtered_data
        report_data.affectedAreaData = filtered_data


    # Ensure the button's on_click function is only assigned once
    if not hasattr(button_report, '_click_event_set'):  # This prevents re-adding the on_click event
        button_report.on_click(call_report_tab)
        button_report._click_event_set = True  # Custom flag to track if it's already set

    # Create a button widget
    button = pn.widgets.Button(name='Query: Polgyon Area', button_type='primary', button_style='solid')
    button.on_click(on_button_click)


    def update_selection_table(new_data):
        table_widget_selectedCountries.value = geo_map_data.countriesData

    geo_map_data.param.watch (update_selection_table,'countriesData')
 
    # It is used to disable editing the table cell - should be updated!
    # for column in df.columns:
    #     column_editor = {
    #         'type': 'editable',  # Set the type to 'editable'
    #         'value': False,      # Set the default value to False
    #     }
    #     tabulator_editors[column] = column_editor

    # Create a Tabulator widget
    table_widget_selectedCountries = pn.widgets.Tabulator(
        value=pd.DataFrame(),
        groupby=['Country', 'Trade Direction'],
        # sizing_mode='stretch_width',
        pagination='remote', # page_size=30,
        width=1100,
        height=500,
        formatters={'Year': '{:,.0f}',},
        header_filters=True,
        editors=tabulator_editors,
    )

    # Create a Tabulator widget
    table_widget_affectedTradeGroups = pn.widgets.Tabulator(
        groupby=['Country', 'Trade Direction'],
        value=pd.DataFrame(),
        # sizing_mode='stretch_width',
        pagination='remote', page_size=30,
        width=1100,
        height=500,
        formatters={'Year': '{:,.0f}'},
        header_filters=True,
        editors=tabulator_editors,
    )


    map_column = pn.Row(pn.Column(pn.pane.Markdown(f"""### <span style="color:#2d5986;">Connection Map for Saxony Trade Groups:</span>"""), combined), pn.Column(pn.pane.Markdown(f"""#### <span style="color:#2d5986;">Selected coordinates:</span>"""), polygon_table, button, button_report,))
    table1 = pn.Column(pn.pane.Markdown(f"""### <span style="color:#2d5986;">Selected Country Data:</span>"""), table_widget_selectedCountries)
    table2 = pn.Column(pn.pane.Markdown(f"""### <span style="color:#2d5986;">Affected Trade Groups and Countries within Polygon Area:</span>"""), table_widget_affectedTradeGroups)

    layout = pn.Column(map_column, table1, table2)
    return layout

