import pandas as pd
import panel as pn

from bokeh.plotting import figure
from bokeh.models.widgets import DateRangeSlider
from bokeh.models import NumeralTickFormatter
from bokeh.themes import built_in_themes
from bokeh.models import FixedTicker
from bokeh.layouts import column, row

from dashboard.plots.helpers import get_report_data

# Retrieve session-specific instances
report_data = get_report_data()

def transform_data_country(df, tradeGroup, tradeDirection, state, countryName, year_range):
    start_year, end_year = year_range
    df = df[(df['tradeGroup'] == tradeGroup) & 
            (df['tradeDirection'] == tradeDirection) & 
            (df['state'] == state) &
            (df['countryName'] == countryName) &
            (df['year'] >= start_year) & 
            (df['year'] <= end_year)]
    return df


def line_plot():
    global line_df
    line_df = report_data.sankeyDF
    
    if line_df.empty or line_df is None:
        return pn.pane.Markdown("Please select a trade group on bar chart to visualize to top 5 country in line plot.")
    else:
        trade_groups = line_df['tradeGroup'].unique()
        if len(trade_groups) == 0 or len(trade_groups) > 1:  # Check if the list is empty
            return pn.pane.Markdown("Please select a trade group on bar chart to visualize to top 5 country in line plot.")
        else:
            selectOutput = pn.widgets.Select(name='Select Output:', options=['Value', 'Weight'], value='Value', width=150)
            
            def create_plot_country(selectOutput, top_five_country):

                def plot(country):
                    countryName = country
                    tradeGroup = line_df['tradeGroup'].unique()[0]
                    state = "Sachsen"
                    tradeDirection = report_data.barChartTradeDirection

                    my_df = report_data.plotData

                    year_min = my_df['year'].min()
                    year_max = my_df['year'].max()
                    year_range = (year_min, year_max)

                    data = transform_data_country(my_df, tradeGroup, tradeDirection, state, countryName, year_range)
                    data['year'] = data['year'].astype(int)
                    data = data.sort_values(by='year')

                    if selectOutput == "Value":
                        x = list(data['year'])
                        y = list(data['totalValue'])
                        TOOLTIPS = [
                                        ('year', "@x"),
                                        ('total value (in Tsd. Euro)', "@y{0,0.0}"),
                                    ]
                    else:
                        x = list(data['year'])
                        y = list(data['totalWeight'])
                        TOOLTIPS = [
                                        ('year', "@x"),
                                        ('total weight (tons)', "@y{0,0.0}"),
                                    ]
                    
                    plot_country = figure(width=720, height=230, x_axis_label="years", y_axis_label="Tsd. Euro", title=f'{tradeGroup} for {countryName}.',tools='hover, reset, wheel_zoom,save', tooltips=TOOLTIPS)
                    # add a line renderer with legend and line thickness
                    plot_country.line(x, y,  line_width=1, line_color="navy",legend_label="Total value") #legend_label="Net Mass in Tons"
                    # Format the y-axis labels with commas
                    plot_country.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
                    plot_country.title.text_font_size = '9pt'
                    plot_country.xaxis.axis_label_text_font_size = "9pt"
                    plot_country.yaxis.axis_label_text_font_size = "9pt"
                    plot_country.xaxis.axis_label_text_font_style = "italic"   
                    plot_country.legend.label_text_font = "times"
                    plot_country.legend.label_text_font_style = "italic"
                    plot_country.legend.label_text_color = "navy"
                    plot_country.legend.location = "top_left"
                    plot_country.xaxis.ticker = FixedTicker(ticks=x)

                    return plot_country

                if selectOutput == "Value":
                    countries =  top_five_country['valueBased']
                else: 
                    countries =  top_five_country['weightBased']


                plots = []
                for country in countries:
                    plot_country = plot(country)
                    plots.append(plot_country)

                return pn.pane.Bokeh(column(*plots))

            top_five_value_countries = line_df.nlargest(5, 'totalValue')[['countryName', 'totalValue']]
            countries_list_v = top_five_value_countries['countryName'].tolist()
            top_five_weight_countries = line_df.nlargest(5, 'totalWeight')[['countryName', 'totalWeight']]
            countries_list_w = top_five_weight_countries['countryName'].tolist()
            top_five_country = {'valueBased': countries_list_v, 'weightBased':countries_list_w}


            create_plot_country(selectOutput=selectOutput, top_five_country=top_five_country)
            bound_plot = pn.bind(create_plot_country, selectOutput=selectOutput, top_five_country=top_five_country)

            layout = pn.Row(
                pn.Column(
                    pn.Spacer(height=15), 
                    bound_plot
                )
            )

            return layout 

