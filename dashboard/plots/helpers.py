import pandas as pd
import param 
import panel as pn


#------------------------------ Main Layout Data ------------------------------# 

class layout_data(param.Parameterized):
    tabs= param.Parameter(default=None)
    report_panel = param.Parameter(default=None)

# Instance of the parameterized class
def get_main_page_layout():
    if not hasattr(pn.state, 'mainPageLayout'):
        pn.state.mainPageLayout = layout_data()
    return pn.state.mainPageLayout


#------------------------------ FOLIUM MAP DYNAMIC VARIABLES ------------------------------# 

class folium_data(param.Parameterized):
    selected_events = param.List()
    newEventListDF= param.DataFrame(default=pd.DataFrame())

# Instance of the parameterized class
def get_disaster_data():
    if not hasattr(pn.state, 'disaster_data'):
        pn.state.disaster_data = folium_data()
    return pn.state.disaster_data

# table_df = concatenated data in dataframe (after query export and import using polygon str)
# selected_events = list of selected events in widgets (Eartquake in Turkey etc.)
# newEventListDF = list of events after disaster query (All Eeartquake events after disaster selection)



#------------------------------ GEO_MAP DYNAMIC VARIABLES ------------------------------# 

class mapData(param.Parameterized):
    polygonCoordinates= param.DataFrame(default=pd.DataFrame(), doc="A sample pandas dataframe to store polygon area coordinates.")
    countriesData= param.DataFrame(default=pd.DataFrame(), doc="A sample pandas dataframe to store the data of selected points on the map.")
    coordStr = param.String(doc="A simple string to store polygon coordinates as a string to use in queries.")

# Instance of the parameterized class
def get_geo_map_data():
    if not hasattr(pn.state, 'geo_map_data'):
        pn.state.geo_map_data = mapData()
    return pn.state.geo_map_data




#------------------------------ REPORT TAB DYNAMIC VARIABLES ------------------------------# 

class reportData(param.Parameterized):
    # observation data (main query)
    mainData= param.DataFrame(default=pd.DataFrame(), doc="A pandas dataframe to store observation data for report.")
    
    # updated by sankey.py and used by line.py
    sankeyDF= param.DataFrame(default=pd.DataFrame(), doc="A pandas dataframe to store sankey diagram data filtered by trade group for report.")
    
    # used by line.py and updated dynamically by test.py via sidebar widgets
    plotData= param.DataFrame(default=pd.DataFrame(), doc="A pandas dataframe to store observation data for report.")

    # Variable(s) from geo_map.py
    affectedAreaData = param.DataFrame(default=pd.DataFrame(), doc="A pandas dataframe to store the affected area data for report.")
    polygonString = param.String(doc="A simple string to store polygon coordinates as a string for report.")
    
    # Variable(s) from test.py
    parametersDict = param.Dict(default={'Trade Direction' : "" , 'Trade Group' : "" , 'State' : "" , 'Year' : "" , 'Weight' : "" , 'Value' : "" }, doc="A sample dictionary to store sidebar widget filters' values for report.")

    # Variable(s) from bar_chart.py
    # used by test.py to watch changes to update sankey flow diagram
    barChartSelectedCategory = param.String(doc="A simple string to store selected trade group label on bar chart for report.")
    # used by line.py
    barChartTradeDirection = param.String(doc="A simple string to store trade direction on bar chart as a string for report.")

    report_map = param.Parameter(default=None)

# Instance of the parameterized class
def get_report_data():
    if not hasattr(pn.state, 'report_data'):
        pn.state.report_data = reportData()
    return pn.state.report_data 




#------------------------------ SANKEY FLOW DIAGRAM DYNAMIC VARIABLES ------------------------------# 

class AppData(param.Parameterized):
    # Dynamic dict to get input_parameters from bar_chart.py to update the actual visualization of sankey diagram dynamically
    my_dict = param.Dict(default={"key1": "value1"}, doc="A sample dictionary is used to store input parameters for sankey flow diagram.")

# Instance of the parameterized class
def get_sankey_data():
    if not hasattr(pn.state, 'sankey_data'):
        pn.state.sankey_data = AppData()
    return pn.state.sankey_data

