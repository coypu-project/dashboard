import panel as pn
pn.extension('plotly')

import pandas as pd
import numpy as np
import re
import warnings
warnings.filterwarnings("ignore") 

from dashboard.queries import query_observation, query_disaster
from dashboard.plots.tabulator import table_plot, tabulator
from dashboard.plots.bar_chart import bar_chart
from dashboard.plots.geo_map import geomap
from dashboard.plots.line import line_plot
from dashboard.plots.folium_map import folium_map, folium_map_initial
from dashboard.plots.sankey import sankey_plot
from dashboard.plots.report import update_report
from dashboard.plots.helpers import get_sankey_data, get_disaster_data, get_report_data, get_main_page_layout
from dashboard.plots.choropleth_map import create_trade_choropleth_dashboard

import asyncio
from concurrent.futures import ThreadPoolExecutor
executor = ThreadPoolExecutor()

# Loading Indicator to show during the new data is loaded in plots
pn.extension(loading_spinner='petal', loading_color='#30b8c7', template='vanilla')
#pn.param.ParamMethod.loading_indicator = True

# Retrieve session-specific instances
report_data = get_report_data()
disaster_data = get_disaster_data()
mainPageLayout = get_main_page_layout()
sankey_data = get_sankey_data()


# Custom CSS to move the loading indicator slightly above the center
custom_css = """
.bk.panel-loading-indicator {
    transform: translate(-50%, -70%) !important;
    top: 50%;
    left: 50%;
    position: fixed;
}
"""
# Custom CSS for active tab color
tab_css = """
.bk-tab.bk-active {
    background-color: #2fb6b6 !important;  /* Green background for active tab */
    color: white !important;               /* White text for active tab */
}

.bk-tab {
    background-color: #f1f1f1;             /* Default background for other tabs */
    color: black !important;               /* Black text for other tabs */
}
"""

# Applying the custom CSS and tab CSS
pn.config.raw_css.append(custom_css)
pn.config.raw_css.append(tab_css)

# Asynchronous function to get or create cached data
async def get_cached_data():
    if 'data' not in pn.state.cache:
        df = await asyncio.get_event_loop().run_in_executor(executor, query_observation)

        def extract_coordinates(point):
            matches = re.findall(r'Point\((.*?)\)', point)
            if matches:
                return matches[0].replace(' ', ', ')

        df = df[(df['totalValue'] != 0.0) | (df['totalWeight'] != 0.0)]
        df['year'] = df['year'].astype(int)
        df['coordinates'] = df['coordinates'].apply(extract_coordinates)
        df = df.drop(columns=['tradeCountryIri'])
        pn.state.cache['data'] = df
    return pn.state.cache['data']

# Create the data frame once and cache it
main_data = asyncio.run(get_cached_data())

# Filter data for selected trade direction input
def filter_data(main_data, direction):
    if 'filteredDataImport' not in pn.state.cache:
        filtered_df_import = main_data[main_data['tradeDirection'] == direction]
        pn.state.cache['filteredDataImport'] = filtered_df_import

    if 'filteredDataExport' not in pn.state.cache:
        filtered_df_export = main_data[main_data['tradeDirection'] == direction]
        pn.state.cache['filteredDataExport'] = filtered_df_export

    if direction == 'export':
        return pn.state.cache['filteredDataExport']
    else:
        return pn.state.cache['filteredDataImport']

 
@pn.io.profile('clustering', engine='snakeviz')
def main():
    global charts_row, report_panel, line_panel, sankey_panel, geomap_panel,bar_chart_panel, folium_map_layout, table_panel
    sankey_panel = None 
    geomap_panel = None 
    bar_chart_panel = None 
    folium_map_layout = None 
    table_panel = None 
    charts_row = None  # Initialize with a placeholder or appropriate initial value
    report_panel = None
    line_panel = None

    # Extracted text from file
    f = open('./resources/sample.md', 'r')
    extracted_text = f.read()

    report_data.mainData = main_data
    unique_trade_groups = sorted(main_data['tradeGroup'].unique())
    
    # Sidebar components
    multi_choice_trade_direction = pn.widgets.MultiSelect(name='Trade Direction', options=['export', 'import'], size=2, value=['import']) #, width=380
    multi_choice_trade_groups = pn.widgets.MultiSelect(name='Trade Group', size=7, options=unique_trade_groups, value = ['Fishing and aquaculture']) #, width=380
    year_range_slider = pn.widgets.RangeSlider(name='Years', start=main_data['year'].min(), end=main_data['year'].max(), value=(2020, 2021), step=1) #, width=200
    regions_selects = pn.widgets.Select(name='State')
    amount_range_slider = pn.widgets.RangeSlider(name='Amount (in tons)',start=main_data['totalWeight'].min() , end=main_data['totalWeight'].max() , value=(main_data['totalWeight'].min(), main_data['totalWeight'].max()), format='0.0a', step =10)
    value_range_slider= pn.widgets.RangeSlider(name='Value (in Tsd. Euro)', start=main_data['totalValue'].min() , end=main_data['totalValue'].max() , value=(main_data['totalValue'].min(), main_data['totalValue'].max()), format='0.0a', step =10)
    # amount_range_slider = pn.widgets.RangeSlider(name='Amount (in tons)', value=(filter_data(main_data, "import")['totalWeight'].min(), filter_data(main_data, "import")['totalWeight'].max()), format='0.0a', step =1000)
    # value_range_slider= pn.widgets.RangeSlider(name='Value (in Tsd. Euro)', value=(filter_data(main_data, "import")['totalValue'].min(), filter_data(main_data, "import")['totalValue'].max()), format='0.0a', step =1000)

    # Function to display the selected option of a widget directly reflected in other widgets as their dynamic input
    @pn.depends(multi_choice_trade_direction.param.value)
    def update_widgets(options):
        if len(options) == 2 or options == []:
            multi_choice_trade_groups.options = sorted(main_data['tradeGroup'].unique())
            year_range_slider.start = main_data['year'].min() # sorted(main_data['year'].unique())[0]
            year_range_slider.end = main_data['year'].max() # sorted(main_data['year'].unique())[-1]
            regions_selects.options = sorted(main_data['state'].unique())
            amount_range_slider.start = main_data['totalWeight'].min()
            amount_range_slider.end = main_data['totalWeight'].max()
            amount_range_slider.step = 1000
            value_range_slider.start = main_data['totalValue'].min()
            value_range_slider.end = main_data['totalValue'].max()
            value_range_slider.step = 1000

        else:
            filtered_data = filter_data(main_data, options[0])
            multi_choice_trade_groups.options = sorted(filtered_data['tradeGroup'].unique())
            year_range_slider.start = filtered_data['year'].min()
            year_range_slider.end = filtered_data['year'].max()
            regions_selects.options = sorted(filtered_data['state'].unique())
            amount_range_slider.start = filtered_data['totalWeight'].min()
            amount_range_slider.end = filtered_data['totalWeight'].max()
            amount_range_slider.step = 1000
            value_range_slider.start = filtered_data['totalValue'].min()
            value_range_slider.end = filtered_data['totalValue'].max()
            value_range_slider.step = 1000


    # Function to filter dynamic dataframe depending on widgets' input interactively and instantly
    @pn.depends(multi_choice_trade_direction.param.value, multi_choice_trade_groups.param.value, year_range_slider.param.value, regions_selects.param.value, amount_range_slider.param.value, value_range_slider.param.value)
    def plots_dataframe(trade_direction_value, trade_groups_value, year_range, regions_value, amount_range, value_range):
        # Convert ranges into lists
        year_range = list(year_range)
        amount_range = list(amount_range)
        value_range = list(value_range)

        # Add a default empty DataFrame to return in case of no data
        empty_df = pd.DataFrame(columns=main_data.columns)

        if len(trade_direction_value) == 2 or trade_direction_value == []:
            if trade_groups_value == []:
                plots_data = main_data[(main_data['year'] >= year_range[0]) & (main_data['year'] <= year_range[1]) &
                                    (main_data['totalWeight'] >= amount_range[0]) & (main_data['totalWeight'] <= amount_range[1]) &
                                    (main_data['totalValue'] >= value_range[0]) & (main_data['totalValue'] <= value_range[1])]
            else:
                plots_data = main_data[(main_data['tradeGroup'].isin(trade_groups_value)) &
                                    (main_data['year'] >= year_range[0]) & (main_data['year'] <= year_range[1]) &
                                    (main_data['totalWeight'] >= amount_range[0]) & (main_data['totalWeight'] <= amount_range[1]) &
                                    (main_data['totalValue'] >= value_range[0]) & (main_data['totalValue'] <= value_range[1])]
        else:
            if trade_groups_value == []:
                plots_data = main_data[(main_data['tradeDirection'] == trade_direction_value[0]) &
                                    (main_data['year'] >= year_range[0]) & (main_data['year'] <= year_range[1]) &
                                    (main_data['totalWeight'] >= amount_range[0]) & (main_data['totalWeight'] <= amount_range[1]) &
                                    (main_data['totalValue'] >= value_range[0]) & (main_data['totalValue'] <= value_range[1])]
            else:
                plots_data = main_data[(main_data['tradeDirection'] == trade_direction_value[0]) &
                                    (main_data['tradeGroup'].isin(trade_groups_value)) &
                                    (main_data['year'] >= year_range[0]) & (main_data['year'] <= year_range[1]) &
                                    (main_data['totalWeight'] >= amount_range[0]) & (main_data['totalWeight'] <= amount_range[1]) &
                                    (main_data['totalValue'] >= value_range[0]) & (main_data['totalValue'] <= value_range[1])]

        # Check if plots_data is empty or None
        if plots_data is None or plots_data.empty:
            return empty_df  # Return an empty DataFrame

        # Update report_data and return valid plots_data
        report_data.plotData = plots_data
        return plots_data

    # Function to store widgets' status under report tab
    @pn.depends(multi_choice_trade_direction.param.value, multi_choice_trade_groups.param.value, year_range_slider.param.value, regions_selects.param.value, amount_range_slider.param.value, value_range_slider.param.value, watch=True)
    def widget_parameters(trade_direction_value, trade_groups_value, year_range, regions_value, amount_range, value_range): # it is not used below at the moment since only Sachsen data is used.
        year_range = list(year_range)
        report_data.parametersDict['Trade Direction'] = trade_direction_value
        report_data.parametersDict['Trade Group'] = trade_groups_value
        report_data.parametersDict['Year'] = year_range
        report_data.parametersDict['State'] = regions_value
        report_data.parametersDict['Weight'] = amount_range
        report_data.parametersDict['Value'] = value_range
           
    # ------------------------------------------------------------------------------------------------------------#
    # Visualization components : Create a reactive version of visualization components that automatically updated #
    # ------------------------------------------------------------------------------------------------------------#

    # Report Tab Layout, it is linked with button_report. Each button click generates the updated report in Report Tab.
    report_panel = pn.Column(update_report("Initial Start"))
    mainPageLayout.report_panel = report_panel

    # Create a button widget
    button_report = pn.widgets.Button(name='Generate Report!', button_type='success', button_style='outline')

    # Define the function that will be called when the button is clicked
    def call_report_tab(event):
        global report_panel
        new_report_panel = update_report("Update")  # Update the report panel
        report_panel.clear()
        report_panel.extend([new_report_panel])
        button_report.on_click(lambda event: tabs.param.update(active=4))

    # Specify the function to be called when the button is clicked
    button_report.on_click(call_report_tab)


    # GeoMap Visualization for "Map" Tab
    geomap_panel = pn.bind(geomap, df=pn.bind(plots_dataframe, 
                                                            trade_direction_value=multi_choice_trade_direction.param.value, 
                                                            trade_groups_value=multi_choice_trade_groups.param.value, 
                                                            year_range=year_range_slider.param.value, 
                                                            regions_value=regions_selects.param.value, 
                                                            amount_range=amount_range_slider.param.value, 
                                                            value_range=value_range_slider.param.value))

    # Bar chart for "Charts" Tab
    bar_chart_panel = pn.bind(bar_chart, df=pn.bind(plots_dataframe, 
                                                            trade_direction_value=multi_choice_trade_direction.param.value, 
                                                            trade_groups_value=multi_choice_trade_groups.param.value, 
                                                            year_range=year_range_slider.param.value, 
                                                            regions_value=regions_selects.param.value, 
                                                            amount_range=amount_range_slider.param.value, 
                                                            value_range=value_range_slider.param.value))

    # Sankey diagram for "Charts" Tab
    # Initial placeholder definition of sankey_panel
    # Example: sankey_panel = pn.panel('')  # or any suitable placeholder
    sankey_panel = pn.bind(sankey_plot, df=pn.bind(plots_dataframe, 
                                                                trade_direction_value=multi_choice_trade_direction.param.value, 
                                                                trade_groups_value=multi_choice_trade_groups.param.value, 
                                                                year_range=year_range_slider.param.value, 
                                                                regions_value=regions_selects.param.value, 
                                                                amount_range=amount_range_slider.param.value, 
                                                                value_range=value_range_slider.param.value)                                                        
                                                                )

    def on_dict_update(event):
        global sankey_panel, charts_row
        #print(f'This is the value of parameters dict: {sankey_data.my_dict.get("trade group")}')
        sankey_panel_new = pn.bind(sankey_plot, df=pn.bind(plots_dataframe, 
                                                                trade_direction_value=multi_choice_trade_direction.param.value, 
                                                                trade_groups_value=multi_choice_trade_groups.param.value, 
                                                                year_range=year_range_slider.param.value, 
                                                                regions_value=regions_selects.param.value, 
                                                                amount_range=amount_range_slider.param.value, 
                                                                value_range=value_range_slider.param.value)                                                        
                                                                )
        sankey_panel = sankey_panel_new
        charts_row[0] = sankey_panel

    # Watch the changes in "input_parameters" dict in sankey.py and update sankey_panel layout accordingly    
    sankey_data.param.watch(on_dict_update, 'my_dict')

    # Initial placeholder definition of line plots
    line_panel = pn.Column(line_plot)

    # Define the function that will be called when the button is clicked and update line plots
    def update_line_plot(event):
        global line_panel
        new_line_panel = line_plot  # Update the report panel
        line_panel.clear()
        line_panel.extend([new_line_panel])

    report_data.param.watch(update_line_plot, 'barChartSelectedCategory')


    table_panel = pn.bind(tabulator, df=pn.bind(plots_dataframe, 
                                                                trade_direction_value=multi_choice_trade_direction.param.value, 
                                                                trade_groups_value=multi_choice_trade_groups.param.value, 
                                                                year_range=year_range_slider.param.value, 
                                                                regions_value=regions_selects.param.value, 
                                                                amount_range=amount_range_slider.param.value, 
                                                                value_range=value_range_slider.param.value)                                                        
                                                                )


    # Initial placeholder definition of folium map
    folium_map_layout = folium_map_initial()

    choropleth_map_panel = create_trade_choropleth_dashboard(report_data.mainData)

    disaster_and_sub_categories = {
        'Biological disaster': ['Epidemic'],
        'Climatological disaster': ['Drought' , 'Glacial lake outburst', 'Wildfire'], 
        'Geophysical disaster': ['Ground movement' , 'Tsunami', 'Volcanic activity'], 
        'Hydrological disaster': ['Avalanche' , 'Flood'], 
        'Meterological disaster' :['Cold wave', 'Hot wave', 'Storm'],
                            }

    # Create a Select widget with nested options
    disaster_selects = pn.widgets.Select(name='Disaster type', groups=disaster_and_sub_categories)
    event_selects = pn.widgets.MultiChoice(name='List of Events', options = [], width = 300)

    eventListDF = query_disaster('Epidemic')
    eventNameList = eventListDF['name'].tolist()
    event_selects.options = eventNameList

    def update_event_list(value):  
        selection = value.new
        selection = selection.title()
        selection = selection.replace(" ","")

        query_response = query_disaster(selection)
        disaster_data.newEventListDF = query_response

        eventNameList = query_response['name'].tolist()
        event_selects.options = eventNameList

    # Link the select widget to the update function
    disaster_selects.param.watch(update_event_list, 'value')

    # Create a button widget
    event_button = pn.widgets.Button(name='Show Events on Map!', button_type='primary', button_style='outline')

    def update_disaster_data(event):
        disaster_data.selected_events = event_selects.value
        folium_map()
        event_button.on_click(lambda event: tabs.param.update(active=3))

    event_button.on_click(update_disaster_data)



    # Create a Tabulator widget to display the dynamic DataFrame, "Test" Tab
    test_dynamic_dataframe = table_plot(plots_dataframe)

    markdown_text = """
    ### <span style="color:#2d5986;">Features:</span>

    - **Bar chart**: Show proportional comparison of trade groups.
    - **Sankey diagram**: Show selected trade group's trade countries. When you select a bar (trade group) in bar chart, it will update sankey diagram.
    - **Line plots**: Show top 5 countries for the selected trade group. 
    - **Table data**: Show the data filtered by sidebar widgets. You can use table's filter widgets to have detailed overview of the data and may download for further analysis.
    """

    bar_chart_panel_title = """ 
    ### <span style="color:#2d5986;">Bar chart:</span>
    - presents a comparative analysis of Trade Groups based on the criteria selected through the sidebar widgets.
    """
    sankey_title = """
    ### <span style="color:#2d5986;">Sankey Flow Diagram:</span>
    - visualizes the connections between countries and the Trade Groups identified 
    in the Bar Chart, based on the current selection.
    """
    line_panel_title = """
    ### <span style="color:#2d5986;">Line Plots:</span>
    - features line plots for the top five countries identified from Sankey Flow Diagram,
    corresponding to the selected Trade Group.
    """
    table_panel_title = """
    ### <span style="color:#2d5986;">Data Table:</span>
    - This table displays the dataset refined by the sidebar filter widgets, offering detailed insights into the selected data.
    """

    # Add image pane
    first_arrow= pn.pane.PNG('https://clipart-library.com/images/6Tyok4ykc.png')
    down_left_arrow = first_arrow.clone(width=300, height=90)

    second_arrow = pn.pane.PNG('https://clipartcraft.com/images/transparent-arrow-curved-2.png')
    right_arrow = second_arrow.clone(width=250, height=90)

    # Charts Tab Panel
    charts_row = pn.Row(sankey_panel, pn.Spacer(width=100), line_panel)
    charts_pane = pn.Column(markdown_text, pn.Spacer(height=20), pn.Column(bar_chart_panel_title, bar_chart_panel, pn.Row(pn.Spacer(width=500),down_left_arrow)), pn.Spacer(height=30), pn.Column(pn.Row(sankey_title,pn.Spacer(width=70), right_arrow, line_panel_title),charts_row), pn.Spacer(height=30), pn.Column(table_panel_title, table_panel))

    # Define the reset button
    select_all_data_button = pn.widgets.Button(name='Select both trade direction and all trade groups!', button_type='primary', button_style='solid')
    reset_events_button = pn.widgets.Button(name='Reset Events!', button_type='danger', button_style='solid')

       
    # Function to select all widgets
    def select_filters(event):
        multi_choice_trade_direction.value = list(multi_choice_trade_direction.options)
        multi_choice_trade_groups.value = list(multi_choice_trade_groups.options) 
        # year_range_slider.value = (main_data['year'].min(), main_data['year'].max())
        #regions_selects.value = None 
        # amount_range_slider.value = (main_data['totalWeight'].min(), main_data['totalWeight'].max())
        # value_range_slider.value = (main_data['totalValue'].min(), main_data['totalValue'].max())

    # Function to reset the selected events data all widgets
    def reset_events(event):
        #disaster_selects.value = disaster_selects.options[0]
        # Check if disaster_selects.options is empty, and set it to the first category from disaster_and_sub_categories if it is
        if not disaster_selects.options:
            # Set the first disaster type available from disaster_and_sub_categories
            first_category = list(disaster_and_sub_categories.keys())[0]  # Get the first key (category)
            first_event = disaster_and_sub_categories[first_category][0]  # Get the first event under that category
            disaster_selects.value = first_event
        else:
            # If options are available, select the first one
            disaster_selects.value = disaster_selects.options[0]
        event_selects.value = []
        disaster_data.selected_events = []
        disaster_data.newEventListDF = pd.DataFrame()
        event_button.clicks += 1


    # Link the button click event to the reset function
    select_all_data_button.on_click(select_filters)

    # Link the button click event to the reset function
    reset_events_button.on_click(reset_events)    


    # Add video pane
    video = pn.pane.Video('https://coypu.org/media/MST_02_CoyPu_Forschungsprojekt_21-036.mp4', width=640, loop=False, time=1.8)

    dataset_information = pn.pane.Markdown("""
        We obtained data from the [Statistisches Bundesamt](https://www.destatis.de/DE/Home/_inhalt.html), encompassing various economic indicators such as foreign trade statistics and the national accounting for both Saxony and Germany. We use SPARQL to gather the following data:

        - **Foreign Trade**  
        Data containing values and tonnage of tradings between Saxony and a country distinguished by trade group, year, month, and trade direction. [Link](https://www-genesis.destatis.de/)

        - **Disaster Events**  
        Disaster events like floods and earthquakes sourced from [EM-DAT](https://public.emdat.be/) and [ReliefWeb](https://reliefweb.int/).

        - **Administrative Region**  
        This entity is sourced from [GeoBoundaries](https://www.geoboundaries.org/) and contains a hierarchical collection of administrative regions of the world (e.g., District of Leipzig is part of Saxony, and Saxony is part of Germany).
        """)

    # Tabs for visualization
    data_tabs = pn.Tabs(('Observation Data "Main Query Data"', table_plot(main_data)), ('Filtered Data by Sidebar Widgets', test_dynamic_dataframe), ('Data Sources', dataset_information),   
                        dynamic=True, styles={'background': '#E0F7FA'})
    
    maps_tab = pn.Tabs(('Connection Map', geomap_panel), ('Choropleth Map', choropleth_map_panel))

    tabs = pn.Tabs(('Overview', pn.Column(extracted_text, video)), ('Charts', charts_pane), ('Maps', maps_tab), ('Disaster & Events', folium_map_layout), ('Report', report_panel), ('Dataset Information', data_tabs), dynamic=False)
    # tabs = pn.Tabs(('Overview', pn.Column(extracted_text, video)), ('Charts', charts_pane), ('Map', geomap_panel), ('Choropleth Map', choropleth_map_panel), ('Disaster & Events', folium_map_layout), ('Report', report_panel), ('Data', data_tabs), dynamic=False)
    # dynamic=True >> enables lazy-loading, which means tabs's data is loaded when they are selected or activated
    #tabs[0].dynamic = False # Load Overview Tab directly
    #tabs[4].dynamic = False # Load Reports Tab directly

    mainPageLayout.tabs = tabs

    # Combine sidebar components
    sidebar = pn.Column(
        f"""## <span style="color:#2d5986;">Interactive Filters for Data Exploration:</span>""",
        pn.Spacer(height=7),
        f"""### <span style="color:#2d5986;">Foreign Trades:</span>""",
        multi_choice_trade_direction,
        multi_choice_trade_groups,
        year_range_slider,
        # regions_selects,
        amount_range_slider,
        value_range_slider,
        select_all_data_button,
        update_widgets,
        pn.Spacer(height=7),
        f"""### <span style="color:#2d5986;">Disasters & Events:</span>""",
        disaster_selects,
        event_selects,
        pn.Row(event_button, pn.Spacer(width=20),reset_events_button),
        pn.Spacer(height=10),
        pn.Row(button_report)
    )

    # URLs and HTML content for the logos and text box
    first_logo_url = "https://infai.org/wp-content/uploads/2022/08/InfAI-Logo-mobile-sm.png"  # URL of your first (new) logo
    first_logo_link_url = "https://infai.org/"  # URL the first logo should link to
    second_logo_url = "https://www.inf.uni-hamburg.de/31790213/coypu-logo-ce4cb0f14a5ebb766535cd158ee112d73cfaf3bc.png"  # URL of your second logo
    second_logo_link_url = "https://coypu.org/"  # URL the second logo should link to
    title_text = ""
    text = "Impressum, CoyPu, BMWK"
    text_link_url = "https://coypu.org/impressum"  # URL the text should link to

    # Combining title, text box, and logos in HTML
    header_html = f'''
    <div style="display: flex; align-items: center; justify-content: space-between;">
        <div style="font-size: 24px; font-weight: bold; color: #000d1a;">{title_text}</div>
        <div style="margin-left: 20px;">
            <a href="{text_link_url}" target="_blank">{text}</a>
        </div>
        <div>
            <a href="{first_logo_link_url}" target="_blank">
                <img src="{first_logo_url}" alt="First Logo" style="height: 50px; margin-right: 10px;">
            </a>
            <a href="{second_logo_link_url}" target="_blank">
                <img src="{second_logo_url}" alt="Second Logo" style="height: 50px;">
            </a>
        </div>
    </div>
    '''
    bmwk_logo = 'https://coypu.org/assets/img/footer-logo/BMWi_Fz_2017_Office_Farbe_de.png'
    
    footer_html = f"""
    <div style="text-align: center;">
        <img src="{bmwk_logo}" alt="Footer Image" style="width: 100px; height: auto;">
    </div>
    """

    # Creating a pane for the custom header
    header_pane = pn.pane.HTML(header_html, sizing_mode='stretch_width')
    footer_pane = pn.pane.HTML(footer_html, style={'padding': '10px', 'background-color': '#f8f9fa'})

    # Create a panel template
    template = pn.template.VanillaTemplate(title='SAXONIAN PUBLIC RESILIENCE DASHBOARD DEMONSTRATOR', sidebar_width = 350, header_color ='#000d1a' , header_background = '#5cc2d6') # #0088cc #D7EEFF , theme ='dark' , '#151B54'
    template.header.append(header_pane) 
    template.sidebar.append(sidebar)
    template.main.append(tabs)
    template.main.append(footer_pane)
    return template 

pn.serve(main, port=5015, show=False, num_procs=4)