# New public dashboard for Coypu

This dashboard uses Python 3.10 and [Panel](https://panel.holoviz.org/) to visualize Foreign Trade Data between Saxony/Germany and the world. Next to trade data, disaster events and potientially affected countries will be taken into account in some widgets, too.

## Demo

An online version of this dashboard is available [here](https://dashboard.coypu.org/).

## Local deploy
To run a local version of the dashboard you need access to the SPARQL endpoint of [Coypu Knowledge Graph](https://coypu.org/ergebnisse/knowledge-graph). If you do not have access, yet, feel free to contact us here.

### Preliminaries
- Python 3.10
- pip or a package/dependency managenent system of your choice

### Installation
- clone this part of the repository with `git clone https://gitlab.com/coypu-project/dashboard.git`
- change directory with `cd dashboard`
- optional but recommended: create virtual environment for python
- install dependencies given in requirements.txt e.g.: `pip install -r requirements.txt`
- copy the distributed .env.dist to .env: `cp .env.dist .env`
- open `.env` and set the variables accordingly
- start the dashboard with `panel serve test.py --autoreload`